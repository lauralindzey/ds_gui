//
// Created by ivaughn on 2/4/19.
//

#include "ds_rqt_widgets/QtRosSubscriptionManager.h"

namespace ds_rqt_widgets {

QtRosSubscriptionManager::QtRosSubscriptionManager(ros::NodeHandle& nh) : node_handle(nh) {
  // do nothing
}

QtRosSubscriptionManager::~QtRosSubscriptionManager() {
  for (auto sub : subscriptions) {
    delete sub;
  }
  subscriptions.clear();
}

void QtRosSubscriptionManager::addSubscription(QtRosSubscription *sub) {
  subscriptions.push_back(sub);
}

void QtRosSubscriptionManager::shutdown() {
  for (auto sub : subscriptions) {
    sub->shutdown();
  }
}

void QtRosSubscriptionManager::saveSettings(qt_gui_cpp::Settings &plugin_settings,
                                            qt_gui_cpp::Settings &instance_settings) const {
  for (auto sub: subscriptions) {
    QString setting_name = topicSettingName(sub);
    std::cout <<"Saving \"" <<setting_name.toStdString() <<"\" = \"" <<sub->getTopicName() <<"\"" <<std::endl;
    instance_settings.setValue(setting_name, QString::fromStdString(sub->getTopicName()));
  }
}

void QtRosSubscriptionManager::restoreSettings(const qt_gui_cpp::Settings &plugin_settings,
                                               const qt_gui_cpp::Settings &instance_settings) {
  // NOTE: This will NOT initialize a list of subscriptions based on the instance settings.  All it
  // can do is initialize the topics for an existing set of subscriptions.
  for (auto sub: subscriptions) {
    QString setting_name = topicSettingName(sub);
    std::string value = instance_settings.value(setting_name,
        QString::fromStdString(sub->getTopicName())).toString().toStdString();

    std::cout <<"Loaded \"" <<setting_name.toStdString() <<"\" = \"" <<value <<"\"" <<std::endl;
    sub->setTopicName(node_handle, node_handle.resolveName(value));
  }
}

const std::vector<QtRosSubscription*>& QtRosSubscriptionManager::getSubscriptions() const {
  return subscriptions;
}

QString QtRosSubscriptionManager::topicSettingName(const QtRosSubscription * const sub) const {
  std::string sub_name = sub->getName();
  std::replace(sub_name.begin(), sub_name.end(),' ', '_');
  return QString::fromStdString("sub_mgr_topic_" + sub_name);
}

}