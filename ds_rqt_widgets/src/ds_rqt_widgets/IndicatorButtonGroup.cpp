//
// Created by ivaughn on 11/4/20.
//

#include <ds_rqt_widgets/IndicatorButtonGroup.h>

namespace ds_rqt_widgets {

IndicatorButtonGroup::IndicatorButtonGroup(const std::string& long_name, const std::string& short_name,
                                           std::function<void(int)>cb, QWidget* parent)
    : QGroupBox(QString::fromStdString(long_name), parent) {
  groupName = short_name;
  callback = cb;

  layout = new QGridLayout();
  this->setLayout(layout);
};

void IndicatorButtonGroup::updateMode(int value) {
  for (auto& but : buttons) {
    if (but.first == value) {
      but.second->setState(IndicatorButton::Status::GOOD);
    } else {
      but.second->setState(IndicatorButton::Status::OFF);
    }
  }
}

void IndicatorButtonGroup::setUnknown() {
  for (auto& but : buttons) {
    but.second->setState(IndicatorButton::Status::UNKNOWN);
  }
}

void IndicatorButtonGroup::addButton(const std::string& name, int enumval) {
  auto b = new IndicatorButton(QString::fromStdString(groupName + "\n" + name), this);

  int r = buttons.size()/2;
  int c = buttons.size() - r*2;
  layout->addWidget(b, r, c);

  buttons.push_back(std::make_pair(enumval, b));
  connect(b, &IndicatorButton::released, [this, enumval](){ this->callback(enumval);});
}

} // namespace ds_rqt_widgets
