//
// Created by ivaughn on 2/22/19.
//

#include <ds_control_msgs/JoystickEnum.h>
#include <ds_control_msgs/ControllerEnum.h>
#include <ds_control_msgs/RovAllocationEnum.h>
#include "ds_rqt_widgets/RovControlSwitcher.h"
#include <ds_control_msgs/RovAutoCommand.h>

namespace ds_rqt_widgets {

RovControlSwitcher::RovControlSwitcher(QWidget* parent) : QWidget(parent)
{
  setupGui();
}

void RovControlSwitcher::setupConnections(
    const ds_param::IntParam::Ptr& _joystick,
    const ds_param::IntParam::Ptr& _controller,
    const ds_param::IntParam::Ptr& _allocator) {
  active_joystick = _joystick;
  active_controller = _controller;
  active_allocator = _allocator;

  auto_enabled = false;
  auto_received = false;
}

IndicatorButtonGroup* RovControlSwitcher::joysticks() {
  return butsJoysticks;
}

IndicatorButtonGroup* RovControlSwitcher::controllers() {
  return butsControllers;
}

IndicatorButtonGroup* RovControlSwitcher::allocations() {
  return butsAllocations;
}

void RovControlSwitcher::setupGui() {
  // Abort/enable modes
  butEnable = new IndicatorButton(QString::fromStdString("Enable"), this);
  butAbort = new IndicatorButton(QString::fromStdString("Abort"), this);
  layoutAbort = new QGridLayout();
  layoutAbort->addWidget(butEnable, 0, 0, 1, 2);
  layoutAbort->addWidget(butAbort, 1, 0, 1, 2);
  groupAbort = new QGroupBox(QString::fromStdString("Abort"), this);
  groupAbort->setLayout(layoutAbort);

  connect(butAbort, &IndicatorButton::released, [this]() {
    // abort manager can't reset, so aborts are always aborts.
    emit this->setVehicleAbort(true);
  });
  connect(butEnable, &IndicatorButton::released, [this]() {
    // toggle state, but if unsure send disable
    emit this->setVehicleEnable(this->butEnable->getState() == IndicatorButton::BAD);
  });

  // auto modes (ROV Controller only)
  butAutoOff = new IndicatorButton(QString::fromStdString("Auto\nOFF"), this);
  butAutoXY = new IndicatorButton(QString::fromStdString("Auto\nXY"), this);
  butAutoHdg = new IndicatorButton(QString::fromStdString("Auto\nHDG"), this);
  butAutoDepth = new IndicatorButton(QString::fromStdString("Auto\nDEP"), this);

  layoutAutos = new QGridLayout();
  layoutAutos->addWidget(butAutoXY, 0, 0);
  layoutAutos->addWidget(butAutoHdg, 0, 1);
  layoutAutos->addWidget(butAutoDepth, 1, 0);
  layoutAutos->addWidget(butAutoOff, 1, 1);
  connect(butAutoXY, &IndicatorButton::released, this, &RovControlSwitcher::callbackAutoXy);
  connect(butAutoHdg, &IndicatorButton::released, this, &RovControlSwitcher::callbackAutoHdg);
  connect(butAutoDepth, &IndicatorButton::released, this, &RovControlSwitcher::callbackAutoDep);
  connect(butAutoOff, &IndicatorButton::released, this, &RovControlSwitcher::callbackAutoOff);

  groupAutos = new QGroupBox(QString::fromStdString("Autos"), this);
  groupAutos->setLayout(layoutAutos);

  // active joysticks
  butsJoysticks = new IndicatorButtonGroup("Joysticks", "Joy",
      [this](int v) { this->setJoystick(v); }, this);
  butsJoysticks->addButton("GAME", ds_control_msgs::JoystickEnum::JOY);
  butsJoysticks->addButton("CONTEST", ds_control_msgs::JoystickEnum::CONTEST);
  butsJoysticks->addButton("ROV", ds_control_msgs::JoystickEnum::ROV);

  // controllers
  butsControllers = new IndicatorButtonGroup("Controllers", "Con",
                                           [this](int v) { this->setController(v); }, this);
  butsControllers->addButton("MAN", ds_control_msgs::ControllerEnum::MANUAL);
  butsControllers->addButton("ROV", ds_control_msgs::ControllerEnum::ROV);

  // allocations
  butsAllocations = new IndicatorButtonGroup("Allocators", "Alloc",
                                             [this](int v) { this->setAllocation(v); }, this);
  butsAllocations->addButton("IDLE", ds_control_msgs::RovAllocationEnum::IDLE);
  butsAllocations->addButton("ROV", ds_control_msgs::RovAllocationEnum::ROV);

  // bringin it all together
  widgetLayout = new QVBoxLayout(this);
  widgetLayout->addWidget(groupAbort);
  widgetLayout->addWidget(butsJoysticks);
  widgetLayout->addWidget(butsControllers);
  widgetLayout->addWidget(butsAllocations);
  widgetLayout->addWidget(groupAutos);
}

void RovControlSwitcher::updateAbort(AnyMessage raw_msg) {
  auto abort = raw_msg.as<ds_core_msgs::Abort>().operator*();

  bool origState = butAbort->blockSignals(true);
  if (!abort.abort) {
    butAbort->setState(IndicatorButton::Status::GOOD);
  } else {
    butAbort->setState(IndicatorButton::Status::BAD);
  }
  butAbort->blockSignals(origState);

  origState = butEnable->blockSignals(true);
  if (abort.enable) {
    butEnable->setState(IndicatorButton::Status::GOOD);
  } else {
    butEnable->setState(IndicatorButton::Status::BAD);
  }
  butEnable->blockSignals(origState);
}

void RovControlSwitcher::updateControllerState(AnyMessage raw_msg) {
  auto_state = raw_msg.as<ds_control_msgs::RovControllerState>().operator*();
  auto_received = true;
  updateAutos();
}

void RovControlSwitcher::updateModes() {
  butsJoysticks->setUnknown();
  if (active_joystick) {
    butsJoysticks->updateMode(active_joystick->Get());
  }

  butsControllers->setUnknown();
  if (active_controller) {
    butsControllers->updateMode(active_controller->Get());
  }

  butsAllocations->setUnknown();
  if (active_allocator) {
    butsAllocations->updateMode(active_allocator->Get());
  }

  // autos only work in ROV && not idle
  if (active_controller && active_allocator) {
    if (active_controller->Get() == ds_control_msgs::ControllerEnum::ROV
          && active_allocator->Get() != ds_control_msgs::RovAllocationEnum::IDLE) {
      enableAutos();
    } else {
      disableAutos();
    }
  } else {
    disableAutos();
  }
}

void RovControlSwitcher::enableAutos() {
  auto_enabled = true;
  auto_received = false;

  updateAutos();
}

void RovControlSwitcher::disableAutos() {
  auto_enabled = false;
  auto_received = false;

  updateAutos();
}

void RovControlSwitcher::updateAutos() {

  if (!auto_enabled) {
    butAutoOff->setState(IndicatorButton::Status::DISABLED);
    butAutoXY->setState(IndicatorButton::Status::DISABLED);
    butAutoHdg->setState(IndicatorButton::Status::DISABLED);
    butAutoDepth->setState(IndicatorButton::Status::DISABLED);
  } else if (!auto_received) {
    butAutoOff->setState(IndicatorButton::Status::OFF);
    butAutoXY->setState(IndicatorButton::Status::UNKNOWN);
    butAutoHdg->setState(IndicatorButton::Status::UNKNOWN);
    butAutoDepth->setState(IndicatorButton::Status::UNKNOWN);
  } else {
    butAutoOff->setState(IndicatorButton::Status::OFF);

    // auto XY
    if (auto_state.autos.auto_xy_enabled) {
      butAutoXY->setState(IndicatorButton::Status::GOOD);
    } else if (auto_state.autos.auto_xy_available) {
      butAutoXY->setState(IndicatorButton::Status::OFF);
    } else {
      butAutoXY->setState(IndicatorButton::Status::DISABLED);
    }

    // auto Depth
    if (auto_state.autos.auto_depth_enabled) {
      butAutoDepth->setState(IndicatorButton::Status::GOOD);
    } else if (auto_state.autos.auto_depth_available) {
      butAutoDepth->setState(IndicatorButton::Status::OFF);
    } else {
      butAutoDepth->setState(IndicatorButton::Status::DISABLED);
    }

    // auto Heading
    if (auto_state.autos.auto_heading_enabled) {
      butAutoHdg->setState(IndicatorButton::Status::GOOD);
    } else if (auto_state.autos.auto_heading_available) {
      butAutoHdg->setState(IndicatorButton::Status::OFF);
    } else {
      butAutoHdg->setState(IndicatorButton::Status::DISABLED);
    }
  }
}

void RovControlSwitcher::setJoystick(int joy) {
  if (active_joystick) {
    active_joystick->Set(joy);
    updateModes();
  }
}

void RovControlSwitcher::setController(int con) {
  if (active_controller) {
    active_controller->Set(con);
    updateModes();
  }
}

void RovControlSwitcher::setAllocation(int alloc) {
  if (active_allocator) {
    active_allocator->Set(alloc);
    updateModes();
  }
}

void RovControlSwitcher::setupAutoService(const ros::ServiceClientPtr &client) {
  auto_service = client;
}

void RovControlSwitcher::callbackAutoXy() {
  if (auto_service) {
    ds_control_msgs::RovAutoCommand cmd;
    if (butAutoXY->getState() != ds_rqt_widgets::IndicatorButton::GOOD) {
      cmd.request.enable_auto_xy = ds_control_msgs::RovAutoCommand::Request::TURN_ON;
      std::cout <<"Auto XY ON!" <<std::endl;
    } else {
      cmd.request.enable_auto_xy = ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
      std::cout <<"Auto XY OFF!" <<std::endl;
    }
    cmd.request.enable_auto_depth   = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;
    cmd.request.enable_auto_heading = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;

    if (auto_service->call(cmd)) {
      if (!cmd.response.success) {
        ROS_ERROR_STREAM("Unable to turn on Auto-XY because \""<<cmd.response.msg <<"\"");
      }
    } else {
      ROS_ERROR_STREAM("Unable to call controller Auto service!");
    }
  }
}

void RovControlSwitcher::callbackAutoDep() {
  if (auto_service) {
    ds_control_msgs::RovAutoCommand cmd;
    if (butAutoDepth->getState() != ds_rqt_widgets::IndicatorButton::GOOD) {
      cmd.request.enable_auto_depth = ds_control_msgs::RovAutoCommand::Request::TURN_ON;
      std::cout <<"Auto Depth ON!" <<std::endl;
    } else {
      cmd.request.enable_auto_depth = ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
      std::cout <<"Auto Depth OFF!" <<std::endl;
    }
    cmd.request.enable_auto_xy      = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;
    cmd.request.enable_auto_heading = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;

    if (auto_service->call(cmd)) {
      if (!cmd.response.success) {
        ROS_ERROR_STREAM("Unable to turn on Auto-XY because \""<<cmd.response.msg <<"\"");
      }
    } else {
      ROS_ERROR_STREAM("Unable to call controller Auto service!");
    }
  }
}

void RovControlSwitcher::callbackAutoHdg() {
  if (auto_service) {
    ds_control_msgs::RovAutoCommand cmd;
    if (butAutoHdg->getState() != ds_rqt_widgets::IndicatorButton::GOOD) {
      cmd.request.enable_auto_heading = ds_control_msgs::RovAutoCommand::Request::TURN_ON;
      std::cout <<"Auto Heading ON!" <<std::endl;
    } else {
      cmd.request.enable_auto_heading = ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
      std::cout <<"Auto Heading OFF!" <<std::endl;
    }
    cmd.request.enable_auto_xy      = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;
    cmd.request.enable_auto_depth   = ds_control_msgs::RovAutoCommand::Request::NO_CHANGE;

    if (auto_service->call(cmd)) {
      if (!cmd.response.success) {
        ROS_ERROR_STREAM("Unable to turn on Auto-XY because \""<<cmd.response.msg <<"\"");
      }
    } else {
      ROS_ERROR_STREAM("Unable to call controller Auto service!");
    }
  }

}

void RovControlSwitcher::callbackAutoOff() {
  if (auto_service) {
    ds_control_msgs::RovAutoCommand cmd;
    cmd.request.enable_auto_xy      = ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
    cmd.request.enable_auto_depth   = ds_control_msgs::RovAutoCommand::Request::TURN_OFF;
    cmd.request.enable_auto_heading = ds_control_msgs::RovAutoCommand::Request::TURN_OFF;

    std::cout <<"All Autos OFF!" <<std::endl;

    if (auto_service->call(cmd)) {
      if (!cmd.response.success) {
        ROS_ERROR_STREAM("Unable to turn on Auto-XY because \""<<cmd.response.msg <<"\"");
      }
    } else {
      ROS_ERROR_STREAM("Unable to call controller Auto service!");
    }
  }

}

}