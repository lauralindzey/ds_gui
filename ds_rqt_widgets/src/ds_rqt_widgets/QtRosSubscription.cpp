//
// Created by ivaughn on 2/1/19.
//

#include "ds_rqt_widgets/QtRosSubscription.h"


// ///////////////////////////////////////////////////////////////////////// //
// AnyMessage
// ///////////////////////////////////////////////////////////////////////// //
// even though its pure virtual, we still need an implementation
AnyMessage::~AnyMessage() {};
const boost::typeindex::type_info &AnyMessage::type() const noexcept {
  return payload.type();
}

namespace ds_rqt_widgets {

// ///////////////////////////////////////////////////////////////////////// //
// GenericSubscription
// ///////////////////////////////////////////////////////////////////////// //
GenericSubscription::GenericSubscription(const std::string& _name) : name(_name) {}
GenericSubscription::~GenericSubscription() {
  subscription.shutdown();
}

void GenericSubscription::setCallback(const boost::function<void(AnyMessage)> &cb) {
  callback = cb;
}

void GenericSubscription::shutdown() {
  subscription.shutdown();
}

std::string GenericSubscription::getTopicName() const {
  return subscription.getTopic();
}

/// This returns the name of the subscription, which should describe
/// the role of this subscription in the GUI
const std::string& GenericSubscription::getName() const {
  return name;
}

uint32_t GenericSubscription::getNumPublishers() const {
  return subscription.getNumPublishers();
}

ros::WallTime GenericSubscription::getLastMessageTime() const {
  return last_message_time;
}

// ///////////////////////////////////////////////////////////////////////// //
// QtRosSubscription
// ///////////////////////////////////////////////////////////////////////// //

QtRosSubscription::QtRosSubscription(GenericSubscription *s) : sub(s) {
  sub->setCallback(boost::bind(&QtRosSubscription::on_data, this, _1));
}

QtRosSubscription::~QtRosSubscription() {
  delete (sub);
}

std::vector<std::string> QtRosSubscription::getTopics(ros::NodeHandle& nh) const {
  return sub->getTopics(nh);
}

std::string QtRosSubscription::getName() const {
  return sub->getName();
}

std::string QtRosSubscription::getTopicName() const {
  return sub->getTopicName();
}

void QtRosSubscription::setTopicName(ros::NodeHandle& nh, const std::string& topicname, uint32_t queue_size) {
  ROS_INFO_STREAM(topicname <<" --> " <<nh.resolveName(topicname));
  sub->setTopicName(nh, nh.resolveName(topicname), queue_size);
}

void QtRosSubscription::shutdown() {
  sub->shutdown();
}

uint32_t QtRosSubscription::getNumPublishers() const {
  return sub->getNumPublishers();
}

ros::WallTime QtRosSubscription::getLastMessageTime_ros() const {
  return sub->getLastMessageTime();
}

QDateTime QtRosSubscription::getLastMessageTime_QT() const {
  ros::WallTime last = sub->getLastMessageTime();

  QDateTime ret;
  ret.setMSecsSinceEpoch(last.toNSec() / 1000000);
  return ret;
}

/// Callback triggered by the subscription object
void QtRosSubscription::on_data(AnyMessage msg) {
  emit received(msg);
}

}