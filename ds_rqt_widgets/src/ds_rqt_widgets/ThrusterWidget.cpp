//
// Created by ivaughn on 3/1/19.
//

#include "ds_rqt_widgets/ThrusterWidget.h"

#include <iostream>

namespace ds_rqt_widgets {

const static int SIZE = 100;

ThrusterWidget::ThrusterWidget(QWidget *parent) : QWidget(parent) {
  max_ = 0;
  min_ = 0;
  center_ = 0;
  orientation_ = POS_IS_RIGHT;
  pos_is_pos_ = true;

  actual_ = 0;
  command_ = 0;
}

ThrusterWidget::ThrusterWidget(ThrusterWidget::Orientation orientation,
    QWidget* parent) : ThrusterWidget(parent) {
  orientation_ = orientation;
}

double ThrusterWidget::getMax() const {
  return max_;
}

double ThrusterWidget::getCenter() const {
  return center_;
}

double ThrusterWidget::getMin() const {
  return min_;
}

double ThrusterWidget::getActual() const {
  return actual_;
}

double ThrusterWidget::getCommand() const {
  return command_;
}

bool ThrusterWidget::isPosPos() const {
  return pos_is_pos_;
}

ThrusterWidget::Orientation ThrusterWidget::getOrientation() const {
  return orientation_;
}

void ThrusterWidget::setMax(double max) {
  max_ = max;
}

void ThrusterWidget::setCenter(double center) {
  center_ = center;
}

void ThrusterWidget::setMin(double min) {
  min_ = min;
}

void ThrusterWidget::setActual(double actual) {
  actual_ = actual;
  update();
}

void ThrusterWidget::setCommand(double command) {
  command_ = command;
  update();
}

void ThrusterWidget::setThrusterEnabled(bool enabled) {
  this->setEnabled(enabled);
}

void ThrusterWidget::setPosIsPos(bool value) {
  pos_is_pos_ = value;
}

void ThrusterWidget::setOrientation(ThrusterWidget::Orientation orientation) {
  orientation_ = orientation;
}

void ThrusterWidget::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
}

void ThrusterWidget::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  draw(painter);
  QWidget::paintEvent(event);
}

QSize ThrusterWidget::sizeHint() const {
  if (orientation_ == POS_IS_LEFT || orientation_ == POS_IS_RIGHT) {
    return QSize(SIZE, SIZE/5);
  }
  return QSize(SIZE/5, SIZE);
}

void ThrusterWidget::draw(QPainter& painter) {

  // compute the percentage to draw
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  painter.setViewport(0, 0, width(), height());
  painter.save();

  switch (orientation_) {
    case POS_IS_RIGHT:
      painter.setWindow(-SIZE/2, -SIZE/10, SIZE, SIZE/5);
      painter.rotate(0);
      break;
    case POS_IS_LEFT:
      painter.setWindow(-SIZE/2, -SIZE/10, SIZE, SIZE/5);
      painter.rotate(180);
      break;
    case POS_IS_UP:
      painter.setWindow(-SIZE/10, -SIZE/2, SIZE/5, SIZE);
      painter.rotate(270);
      break;
    case POS_IS_DOWN:
      painter.setWindow(-SIZE/10, -SIZE/2, SIZE/5, SIZE);
      painter.rotate(90);
      break;
    default:
      qCritical("Unknown ThrusterWidget orientation!");
  }
  QRect win = painter.window();

  // ok, now background
  if (this->isEnabled()) {
    painter.setPen(Qt::white);
    painter.setBrush(Qt::black);
  } else {
    painter.setPen(Qt::lightGray);
    painter.setPen(Qt::darkGray);
  }

  painter.drawRect(-SIZE/2, -SIZE/10, SIZE, SIZE/5);

  // draw the main bar
  painter.setBrush(Qt::blue);
  double pct_value = compute_percent(actual_);
  int len = SIZE/2*fabs(pct_value);
  if (pct_value < 0) {
    painter.drawRect(-len, -SIZE/10, len, SIZE/5);
  } else {
    painter.drawRect(0, -SIZE/10, len, SIZE/5);
  }

  // draw the command indicator
  double cmd_value = compute_percent(command_);
  int cmd_x = round((cmd_value/2)*SIZE);

  QPen pen = painter.pen();
  pen.setWidth(3);
  pen.setColor(Qt::darkRed);
  painter.setPen(pen);

  // draw the
  painter.drawLine(cmd_x, -SIZE/10, cmd_x, SIZE/10);

  // DONE! cleanup
  painter.restore();
}

double ThrusterWidget::compute_percent(double value) {
  // first, clamp to min/max
  value = std::max(min_, value);
  value = std::min(max_, value);

  // flip if so ased
  if (!pos_is_pos_)
    value *= -1.0;

  // center
  value -= center_;

  if (value >= 0) {
    return value / fabs(max_ - center_);
  } else {
    return value / fabs(min_ - center_);
  }
}

}