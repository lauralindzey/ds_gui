//
// Created by ivaughn on 12/28/19.
//

#include "ds_rqt_widgets/UBloxNavTable.h"

namespace Field {
const static size_t name = 0;

const static size_t longitude = name + 1;
const static size_t latitude = longitude + 1;
const static size_t height_ellps = latitude + 1;
const static size_t height_msl = height_ellps + 1;
const static size_t velocity = height_msl + 1;

const static size_t fixtype = velocity + 2;
const static size_t acc_xyz = fixtype + 1;
const static size_t acc_vel = acc_xyz + 1;
const static size_t acc_time = acc_vel + 1;

const static size_t relpos_status = acc_time + 2;
const static size_t relpos_station = relpos_status + 1;
const static size_t relpos_vector = relpos_station + 1;
const static size_t relpos_vec_acc = relpos_vector + 1;
const static size_t relpos_rnghdg = relpos_vec_acc + 1;
const static size_t relpos_rnghdg_acc = relpos_rnghdg + 1;

const static size_t gps_time = relpos_rnghdg_acc + 2;
const static size_t tf_frame = gps_time + 1;
const static size_t latency = tf_frame + 1;
const static size_t interval = latency + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

UbloxNavTable::UbloxNavTable(const std::string& name, QWidget *parent)
    : SensorTableBase(Field::age, name, parent) {
  setup();
}

void UbloxNavTable::updateNav(AnyMessage ubxNav) {
  msg = *(ubxNav.as<ds_sensor_msgs::UbloxNav>());
  updateMsgTime();
  refresh();
}

void UbloxNavTable::refresh() {
  if (!checkRefresh()) {
    return;
  }

  fields[Field::longitude]->setText(QString("%1%2").arg(msg.longitude, 11, 'f', 7)
                                        .arg(QChar(0260)));
  fields[Field::latitude]->setText(QString("%1%2").arg(msg.latitude, 11, 'f', 7)
                                        .arg(QChar(0260)));
  fields[Field::height_ellps]->setText(QString("%1 m").arg(msg.height_ellipsoid, 8, 'f', 3));
  fields[Field::height_msl]->setText(QString("%1 m").arg(msg.height_msl, 8, 'f', 3));
  fields[Field::velocity]->setText(QString("[ %1 %2 %3 ] m/s")
                                         .arg(msg.velocity_north, 7, 'f', 3)
                                         .arg(msg.velocity_east, 7, 'f', 3)
                                         .arg(msg.velocity_down, 7, 'f', 3));

  fields[Field::fixtype]->setText(QString::fromStdString(msg.fixtype));
  fields[Field::acc_xyz]->setText(QString("H: %1 cm V: %2 cm")
                                       .arg(100.0*msg.accuracy_horz, 7, 'f', 2)
                                       .arg(100.0*msg.accuracy_vert, 7, 'f', 2));
  fields[Field::acc_time]->setText(QString("%1 us").arg(msg.accuracy_time*1.0e6, 9, 'f', 5));
  fields[Field::acc_vel]->setText(QString("%1 cm/s").arg(msg.accuracy_vel*100, 9, 'f', 5));

  std::string status_str = "UNKNOWN";
  if (msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_RELPOSVALID) {
    status_str = "Valid";
  } else {
    status_str = "INVALID";
  }
  if (msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_GNSSOK) {
    status_str += " GNSS";
  }
  if (msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_DIFFSOLN) {
    status_str += " Diff";
  }
  int carrSoln = (msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_CARRSOLN_MASK) >> 3;
  switch (carrSoln) {
    case 0:
      status_str += " NoPhase";
      break;
    case 1:
      status_str += " AmbPhase";
      break;
    case 2:
      status_str += " GoodPhase";
      break;
  }
  if (msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_ISMOVING) {
    status_str += " Moving";
  }
  if (msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_REFPOSMISS) {
    status_str += " PosMiss";
  }
  if (msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_REFOBSMISS) {
    status_str += " ObsMiss";
  }
  fields[Field::relpos_status]->setText(QString::fromStdString(status_str));
  fields[Field::relpos_station]->setText(QString("%1").arg(msg.relpos_station_id, 6));
  fields[Field::relpos_vector]->setText(QString("[ %1 %2 %3 ] m")
                                            .arg(msg.relpos_north, 8, 'f', 2)
                                            .arg(msg.relpos_east, 8, 'f', 2)
                                            .arg(msg.relpos_down, 8, 'f', 2));
 fields[Field::relpos_vec_acc]->setText(QString("[ %1 %2 %3 ] cm")
                                            .arg(100.0*msg.relpos_accuracy_north, 8, 'f', 2)
                                             .arg(100.0*msg.relpos_accuracy_east, 8, 'f', 2)
                                             .arg(100.0*msg.relpos_accuracy_down, 8, 'f', 2));
  fields[Field::relpos_rnghdg]->setText(QString("%1 m / %2%3")
                                            .arg(msg.relpos_length, 12, 'f', 4)
                                            .arg(msg.relpos_heading, 9, 'f', 5)
                                            .arg(QChar(0260)));
 fields[Field::relpos_rnghdg_acc]->setText(QString("%1 m / %2%3")
                                                .arg(msg.relpos_accuracy_length, 12, 'f', 4)
                                                .arg(msg.relpos_accuracy_heading, 9, 'f', 5)
                                                .arg(QChar(0260)));

  fields[Field::gps_time]->setText(QString("%1 s")
                                       .arg(static_cast<double>(msg.time_of_week)/1000.0, 10, 'f', 3));

  fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
  ros::Duration latency = msg.ds_header.io_time - msg.header.stamp;
  fields[Field::latency]->setText(QString("%1 ms").arg(latency.toSec()*1000.0, 6, 'f', 1));

  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void UbloxNavTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::longitude] = "Longitude";
  row_labels[Field::latitude] = "Latitude";
  row_labels[Field::height_ellps] = "Height, Ellipsoid";
  row_labels[Field::height_msl] = "Height, MSL";
  row_labels[Field::velocity] = "Velocity";

  row_labels[Field::fixtype] = "Fix Type";
  row_labels[Field::acc_xyz] = "Accuracy, Positioning";
  row_labels[Field::acc_time] = "Accuracy, Time";
  row_labels[Field::acc_vel] = "Accuracy, Velocity";

  row_labels[Field::relpos_status] = "Rel.Pos. Status";
  row_labels[Field::relpos_station] = "Rel.Pos. Station";
  row_labels[Field::relpos_vector] = "Rel.Pos. Vector";
  row_labels[Field::relpos_vec_acc] = "Rel.Pos. Accuracy";
  row_labels[Field::relpos_rnghdg] = "Rel.Pos. Range/Heading";
  row_labels[Field::relpos_rnghdg_acc] = "Rel.Pos. Rng/Hdg Accuracy";

  row_labels[Field::gps_time] = "GPS Time";
  row_labels[Field::tf_frame] = "TF Frame";
  row_labels[Field::latency] = "Latency";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";
  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets