//
// Created by ivaughn on 12/27/19.
//

#include "ds_rqt_widgets/PhinsTable.h"

namespace Field {
const static size_t name = 0;
const static size_t hpr = name+2;
const static size_t hpr_stddev = hpr+ 1;
const static size_t hpr_rate = hpr_stddev+ 1;

const static size_t quaternion = hpr_rate + 2;
const static size_t quaternion_stddev = quaternion + 1;

const static size_t heave = quaternion_stddev + 1;

const static size_t body_rates = heave + 2;
const static size_t body_accel = body_rates + 1;

const static size_t sensor_status = body_accel + 1;
const static size_t algo_status_1 = sensor_status + 1;
const static size_t algo_status_2 = algo_status_1 + 1;
const static size_t system_status = algo_status_2 + 1;
const static size_t user_status = system_status + 1;

const static size_t tf_frame = user_status + 2;
const static size_t latency = tf_frame + 1;
const static size_t interval = latency + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

PhinsTable::PhinsTable(const std::string& name, QWidget *parent)
: SensorTableBase(Field::age, name, parent) {
  setup();
}

void PhinsTable::updatePhinsbin(AnyMessage raw) {
  msg = *(raw.as<ds_sensor_msgs::PhinsStdbin3>());
  updateMsgTime();
  refresh();
}

void PhinsTable::refresh() {
  if (!checkRefresh()) {
    return;
  }

  fields[Field::hpr]->setText(QString("[ %1 %2 %3 ] %4")
                                  .arg(msg.heading, 7, 'f', 3)
                                  .arg(msg.pitch, 7, 'f', 3)
                                  .arg(msg.roll, 7, 'f', 3).arg(QChar(0260)));

  fields[Field::hpr_stddev]->setText(QString("[ %1 %2 %3 ] %4")
                                  .arg(msg.heading_stddev, 7, 'f', 3)
                                  .arg(msg.pitch_stddev, 7, 'f', 3)
                                  .arg(msg.roll_stddev, 7, 'f', 3).arg(QChar(0260)));

  fields[Field::hpr_rate]->setText(QString("[ %1 %2 %3 ] %4/s")
                                  .arg(msg.heading_rate, 7, 'f', 3)
                                  .arg(msg.pitch_rate, 7, 'f', 3)
                                  .arg(msg.roll_rate, 7, 'f', 3).arg(QChar(0260)));

  fields[Field::quaternion]->setText(QString("[ %1 %2 %3 %4]")
                                       .arg(msg.attitude_quaternion[0], 6, 'f', 4)
                                       .arg(msg.attitude_quaternion[1], 6, 'f', 4)
                                       .arg(msg.attitude_quaternion[2], 6, 'f', 4)
                                       .arg(msg.attitude_quaternion[3], 6, 'f', 4));

  fields[Field::quaternion_stddev]->setText(QString("[ %1 %2 %3 ]")
                                         .arg(msg.attitude_quaternion_stddev[0], 6, 'f', 4)
                                         .arg(msg.attitude_quaternion_stddev[1], 6, 'f', 4)
                                         .arg(msg.attitude_quaternion_stddev[2], 6, 'f', 4));

  fields[Field::heave]->setText(QString("%1 m").arg(msg.rt_heave_XVnH[2], 6, 'f', 3));

  fields[Field::body_rates]->setText(QString("[ %1 %2 %3 ] %4/s")
                                       .arg(msg.body_rates_XVn[0], 8, 'f', 4)
                                       .arg(msg.body_rates_XVn[1], 8, 'f', 4)
                                       .arg(msg.body_rates_XVn[2], 8, 'f', 4).arg(QChar(0260)));

  fields[Field::body_accel]->setText(QString("[ %1 %2 %3 ] m/s^2")
                                         .arg(msg.body_accel_XVn[0], 8, 'f', 4)
                                         .arg(msg.body_accel_XVn[1], 8, 'f', 4)
                                         .arg(msg.body_accel_XVn[2], 8, 'f', 4));

  fields[Field::sensor_status]->setText(QString("%1 %2")
                                         .arg(msg.sensor_status[0], 8, 16, QChar('0'))
                                         .arg(msg.sensor_status[1], 8, 16, QChar('0')));

  fields[Field::algo_status_1]->setText(QString("%1 %2")
                                         .arg(msg.ins_algo_status[0], 8, 16, QChar('0'))
                                         .arg(msg.ins_algo_status[1], 8, 16, QChar('0')));

  fields[Field::algo_status_2]->setText(QString("%1 %2")
                                         .arg(msg.ins_algo_status[2], 8, 16, QChar('0'))
                                         .arg(msg.ins_algo_status[3], 8, 16, QChar('0')));

  fields[Field::system_status]->setText(QString("%1 %2 %3")
                                            .arg(msg.ins_system_status[0], 8, 16, QChar('0'))
                                            .arg(msg.ins_system_status[1], 8, 16, QChar('0'))
                                            .arg(msg.ins_system_status[2], 8, 16, QChar('0')));

  fields[Field::user_status]->setText(QString("%1").arg(msg.ins_user_status, 8, 16, QChar('0')));

  fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
  ros::Duration latency = msg.ds_header.io_time - msg.header.stamp;
  fields[Field::latency]->setText(QString("%1 ms").arg(latency.toSec()*1000.0, 6, 'f', 1));

  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void PhinsTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::hpr] = "Hdg/Pitch/Roll";
  row_labels[Field::hpr_stddev] = "Std. Dev.";
  row_labels[Field::hpr_rate] = "Rates";

  row_labels[Field::quaternion] = "Quaternion";
  row_labels[Field::quaternion_stddev] = "Quat. Std.";

  row_labels[Field::heave] = "Heave";

  row_labels[Field::body_rates] = "Body Rates";
  row_labels[Field::body_accel] = "Body Accel";

  row_labels[Field::sensor_status] = "Sensor Status";
  row_labels[Field::algo_status_1] = "Algo Status 1/2";
  row_labels[Field::algo_status_2] = "Algo Status 3/4";
  row_labels[Field::system_status] = "Sys. Status";
  row_labels[Field::user_status] = "User Status";

  row_labels[Field::tf_frame] = "TF Frame";
  row_labels[Field::latency] = "Latency";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";

  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets
