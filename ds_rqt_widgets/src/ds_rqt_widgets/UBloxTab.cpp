//
// Created by ivaughn on 12/28/19.
//

#include "ds_rqt_widgets/UBloxTab.h"

namespace ds_rqt_widgets {

UbloxTab::UbloxTab(const std::string& name, QWidget* parent) : name_(name) {
  setup();
}

void UbloxTab::updateAges() {
  nav_tab_->updateAges();
  svin_tab_->updateAges();
  sat_tab_->updateAges();
}

void UbloxTab::updateNav(AnyMessage ubxNav) {
  nav_tab_->updateNav(ubxNav);
}

void UbloxTab::updateSvin(AnyMessage svin) {
  svin_tab_->updateSvin(svin);
}

void UbloxTab::updateSats(AnyMessage sats) {
  sat_tab_->updateSats(sats);
}

void UbloxTab::updateSigs(AnyMessage sigs) {
  sat_tab_->updateSigs(sigs);
}

void UbloxTab::setup() {
  layout_ = new QGridLayout();

  nav_tab_ = new UbloxNavTable(name_ + " Nav", this);
  svin_tab_ = new UbloxSvInTable(name_ + " SurveyIn", this);
  sat_tab_ = new UbloxSatTable(name_ + " Satellites", this);

  layout_->addWidget(nav_tab_, 0, 0);
  layout_->addWidget(svin_tab_, 0, 1);
  layout_->addWidget(sat_tab_, 0, 2);

  this->setLayout(layout_);
}

} // namespace ds_rqt_widgets
