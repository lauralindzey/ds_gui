//
// Created by ivaughn on 3/1/19.
//

#include "ds_rqt_widgets/RovReferenceTable.h"

#include <QHeaderView>
#include <geometry_msgs/Wrench.h>

namespace ds_rqt_widgets {

// ///////////////////////////////////////////////////////////////////////// //
// RovReferenceTableModel
// ///////////////////////////////////////////////////////////////////////// //
RovReferenceTableModel::RovReferenceTableModel(QWidget* parent)
: QAbstractTableModel(parent)
{
}

int RovReferenceTableModel::rowCount(const QModelIndex &parent) const {
  return 4;
}

int RovReferenceTableModel::columnCount(const QModelIndex &parent) const {
  return 9;
}

QVariant RovReferenceTableModel::data(const QModelIndex &index, int role) const {

  if (role == Qt::DisplayRole) {

    switch (index.column()) {
      case 0: // Mode
        return getState(index.row(), state_);

      case 1: // To Go
        return getDiff(index.row(), navagg_, goal_);

      case 2: // Actual (X)
        return getPos(index.row(), navagg_);

      case 3: // Reference (X)
        return getPos(index.row(), reference_);

      case 4: // Goal (X)
        return getPos(index.row(), goal_);

      case 5: // Actual (vel)
        return getVel(index.row(), navagg_);

      case 6: // Reference (vel)
        return getVel(index.row(), reference_);

      case 7: // Goal (vel)
        return getVel(index.row(), goal_);

      case 8: // Force
        return getForce(index.row(), wrench_);
    }
  } else if (role == Qt::TextAlignmentRole) {
    return Qt::AlignRight;
  }

  return QVariant();
}

QVariant RovReferenceTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
  if (role == Qt::DisplayRole) {
    if (orientation == Qt::Horizontal) {
      switch (section) {
        case 0:return tr("Mode");
        case 1:return tr("To Go");
        case 2:return tr("Actual");
        case 3:return tr("Ref");
        case 4:return tr("Goal");
        case 5:return tr("Actual Rate");
        case 6:return tr("Ref Rate");
        case 7:return tr("Goal Rate");
        case 8:return tr("Force");
        default:return QVariant();
      }
    }
    if (orientation == Qt::Vertical) {
      switch (section) {
        case 0:return tr("North [m]");
        case 1:return tr("East [m]");
        case 2:return tr("Depth [m]");
        case 3:return tr("Heading [deg]");
        default:return QVariant();
      }
    }
  } else if (role == Qt::TextAlignmentRole) {
    return Qt::AlignRight;
  }

  return QVariant();
}

const ds_nav_msgs::AggregatedState& RovReferenceTableModel::getNavagg() const {
  return navagg_;
}

const ds_nav_msgs::AggregatedState& RovReferenceTableModel::getReference() const {
  return reference_;
}

const ds_nav_msgs::AggregatedState& RovReferenceTableModel::getGoal() const {
  return goal_;
}

const geometry_msgs::WrenchStamped& RovReferenceTableModel::getWrench() const {
  return wrench_;
}

void RovReferenceTableModel::updateNavigation(AnyMessage raw_msg) {
  navagg_ = raw_msg.as<ds_nav_msgs::AggregatedState>().operator*();

  QModelIndex startOfPosCol = this->index(0, 2);
  QModelIndex endOfPosCol   = this->index(3, 2);
  emit QAbstractItemModel::dataChanged(startOfPosCol, endOfPosCol);

  QModelIndex startOfVelCol = this->index(0, 5);
  QModelIndex endOfVelCol   = this->index(3, 5);
  emit QAbstractItemModel::dataChanged(startOfVelCol, endOfVelCol);

  updateToGo();
}

void RovReferenceTableModel::updateReference(AnyMessage raw_msg) {
  reference_ = raw_msg.as<ds_nav_msgs::AggregatedState>().operator*();

  QModelIndex startOfPosCol = this->index(0, 3);
  QModelIndex endOfPosCol   = this->index(3, 3);
  emit QAbstractItemModel::dataChanged(startOfPosCol, endOfPosCol);

  QModelIndex startOfVelCol = this->index(0, 6);
  QModelIndex endOfVelCol   = this->index(3, 6);
  emit QAbstractItemModel::dataChanged(startOfVelCol, endOfVelCol);
}

void RovReferenceTableModel::updateGoal(AnyMessage raw_msg) {
  goal_ = raw_msg.as<ds_nav_msgs::AggregatedState>().operator*();

  QModelIndex startOfPosCol = this->index(0, 4);
  QModelIndex endOfPosCol   = this->index(3, 4);
  emit QAbstractItemModel::dataChanged(startOfPosCol, endOfPosCol);

  QModelIndex startOfVelCol = this->index(0, 7);
  QModelIndex endOfVelCol   = this->index(3, 7);
  emit QAbstractItemModel::dataChanged(startOfVelCol, endOfVelCol);

  updateToGo();
}

void RovReferenceTableModel::updateWrench(AnyMessage raw_msg) {
  wrench_ = raw_msg.as<geometry_msgs::WrenchStamped>().operator*();

  QModelIndex startOfForceCol = this->index(0, 8);
  QModelIndex endOfForceCol   = this->index(3, 8);
  emit QAbstractItemModel::dataChanged(startOfForceCol, endOfForceCol);
}

void RovReferenceTableModel::updateControllerState(AnyMessage raw_msg) {
  state_ = raw_msg.as<ds_control_msgs::RovControllerState>().operator*();

  QModelIndex startOfCol = this->index(0, 0);
  QModelIndex endOfCol   = this->index(3, 0);
  emit QAbstractItemModel::dataChanged(startOfCol, endOfCol);
}

void RovReferenceTableModel::updateToGo() {
  QModelIndex startOfCol = this->index(0, 1);
  QModelIndex endOfCol   = this->index(3, 1);
  emit QAbstractItemModel::dataChanged(startOfCol, endOfCol);
}

QVariant RovReferenceTableModel::getPos(int idx,
    const ds_nav_msgs::AggregatedState& state) const {

  ds_nav_msgs::FlaggedDouble value;
  switch (idx) {

    case 0:
      value = state.northing;
      break;

    case 1:
      value = state.easting;
      break;

    case 2:
      value = state.down;
      break;

    case 3:
      value = state.heading;
      value.value *= (180.0/M_PI);
      break;

    default:
      return QVariant();
  }

  if (value.valid) {
    return QString::number(value.value, 'f', 2);
  }
  return tr("---");
}

QVariant RovReferenceTableModel::getVel(int idx,
    const ds_nav_msgs::AggregatedState& state) const {

  ds_nav_msgs::FlaggedDouble value;
  switch (idx) {

    case 0:
      value = state.surge_u;
      break;

    case 1:
      value = state.sway_v;
      break;

    case 2:
      value = state.heave_w;
      break;

    case 3:
      value = state.r;
      value.value *= (180.0/M_PI);
      break;

    default:
      return QVariant();
  }

  if (value.valid) {
    return QString::number(value.value, 'f', 2);
  }
  return tr("---");
}

QVariant RovReferenceTableModel::getDiff(int idx, const ds_nav_msgs::AggregatedState& start,
  const ds_nav_msgs::AggregatedState& end) const {

  ds_nav_msgs::FlaggedDouble value_end, value_start;
  switch(idx) {
    case 0:
      value_end = end.northing;
      value_start = start.northing;
      break;

    case 1:
      value_end = end.easting;
      value_start = start.easting;
      break;

    case 2:
      value_end = end.down;
      value_start = start.down;
      break;

    case 3:
      value_end = end.heading;
      value_start = start.heading;
      break;

    default:
      return QVariant();
  }

  if (!value_start.valid || !value_end.valid) {
    return tr("---");
  }

  double delta = value_end.value - value_start.value;
  if (idx == 3) {
    delta *= (180.0 / M_PI);

    if (delta > 180.0) {
      delta -= 360.0;
    } else if (delta <= -180.0) {
      delta += 360.0;
    }
  }

  return QString::number(delta, 'f', 2);
}

QVariant RovReferenceTableModel::getState(int idx, const ds_control_msgs::RovControllerState& state) const {
  switch (idx) {
    case 0:
      if (state.autos.auto_xy_enabled) {
        return tr("Auto");
      }
      return tr("Thrust");
    case 1:
      if (state.autos.auto_xy_enabled) {
        return tr("Auto");
      }
      return tr("Thrust");
    case 2:
      if (state.autos.auto_depth_enabled) {
        return tr("Auto");
      }
      return tr("Thrust");
    case 3:
      if (state.autos.auto_heading_enabled) {
        return tr("Auto");
      }
      return tr("Thrust");
  }
  return QVariant();
}

QVariant RovReferenceTableModel::getForce(int idx, const geometry_msgs::WrenchStamped& wrench) const {
  // NOTE: Forces come in ROS-style forward/port/up.  We convert to SNAME-style forward/stbd/down because
  // that's what the ENTIRE rest of the table uses.
  switch (idx) {
    case 0:
      return QString::number(wrench.wrench.force.x, 'f', 2);
    case 1:
      return QString::number(-wrench.wrench.force.y, 'f', 2);
    case 2:
      return QString::number(-wrench.wrench.force.z, 'f', 2);
    case 3:
      return QString::number(-wrench.wrench.torque.z, 'f', 2);
  }
  return QVariant();
}

// ///////////////////////////////////////////////////////////////////////// //
// RovReferenceTable
// ///////////////////////////////////////////////////////////////////////// //

RovReferenceTable::RovReferenceTable(QWidget* parent): QWidget(parent),
    model_(new RovReferenceTableModel(this)) {
  tableView_ = new QTableView(this);
  tableView_->setSortingEnabled(false);
  tableView_->setModel(model_);

  layout_ = new QHBoxLayout();
  layout_->addWidget(tableView_);

  this->setLayout(layout_);
}
const ds_nav_msgs::AggregatedState& RovReferenceTable::getNavagg() const {
  return model_->getNavagg();
}

const ds_nav_msgs::AggregatedState& RovReferenceTable::getReference() const {
  return model_->getReference();
}

const ds_nav_msgs::AggregatedState& RovReferenceTable::getGoal() const {
  return model_->getGoal();
}

const geometry_msgs::WrenchStamped& RovReferenceTable::getWrench() const {
  return model_->getWrench();
}

void RovReferenceTable::updateNavigation(AnyMessage raw_msg) {
  model_->updateNavigation(raw_msg);
}

void RovReferenceTable::updateReference(AnyMessage raw_msg) {
  model_->updateReference(raw_msg);
}

void RovReferenceTable::updateGoal(AnyMessage raw_msg) {
  model_->updateGoal(raw_msg);
}

void RovReferenceTable::updateWrench(AnyMessage raw_msg) {
  model_->updateWrench(raw_msg);
}

void RovReferenceTable::updateControllerState(AnyMessage raw_msg) {
  model_->updateControllerState(raw_msg);
}

};