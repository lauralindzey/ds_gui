//
// Created by ivaughn on 12/17/19.
//

#include "ds_rqt_widgets/ClioBatteryModuleTable.h"

// Marks as bad undervoltage, overtemp, terminate charge, and overcharged alarms
const uint16_t STATUS_MASK = 0xf800;

namespace Field {
const static size_t module_name = 0;
const static size_t status_str = 1;
const static size_t fault_str = 2;
const static size_t min_temp = 4;
const static size_t max_temp = 5;
const static size_t min_pack_voltage = 6;
const static size_t max_pack_voltage = 7;
const static size_t min_cell_voltage = 8;
const static size_t max_cell_voltage = 9;
const static size_t voltage = 11;
const static size_t status_code = 12;
const static size_t percentage = 13;
const static size_t remainingCapacity = 14;
const static size_t fullCapacity = 15;
const static size_t designCapacity = 16;
const static size_t chargetimeRemaining = 18;
const static size_t uptime = 19;
const static size_t overflowCount = 20;
const static size_t timeoutCount = 21;
const static size_t startupCondition = 22;
const static size_t date = 23;
const static size_t busAddress = 24;
const static size_t powerswitchState = 25;
const static size_t age = 27;
const static size_t NUM_ROWS = 28;
}

namespace ds_rqt_widgets {

ClioBatteryModuleTable::ClioBatteryModuleTable(int n_bats, QWidget *parent)
: QFrame(parent), num_bats(n_bats) {
  setup();
}

void ClioBatteryModuleTable::updateBattery(AnyMessage clio_bat, int batnum) {
  auto msg = clio_bat.as<ds_hotel_msgs::ClioBattery>();
  if (batnum >= fields.size()) {
    ROS_ERROR_STREAM("Battery message for battery " <<batnum+1 <<" but only " <<fields.size() <<" expected by GUI!");
    return;
  }
  auto& bat = fields[batnum];

  // module name done automagically
  std::string status_str;
  if (msg->charging || msg->chargeEnabled) {
    if (msg->chargeEnabled && !msg->charging) {
      status_str = "Charge FAULT";
    } else {
      status_str = "Charge";
    }
    if (msg->discharging) {
      status_str += "\nDischarge";
    }
  } else if (msg->discharging) {
    status_str = "Discharge";
  } else {
    status_str = "Off";
  }
  bat[Field::status_str]->setText(QString::fromStdString(status_str));

  std::string fault_str;
  if (msg->moduleStatus & 0x0800) {
    fault_str += "OVER DISCHARGE\n";
  }
  if (msg->moduleStatus & 0x1000) {
    fault_str += "OVER TEMP\n";
  }
  if (msg->moduleStatus & 0x4000) {
    fault_str += "STOP CHARGE\n";
  }
  if (msg->moduleStatus & 0x0800) {
    fault_str += "OVER CHARGE\n";
  }
  if (msg->moduleStatus & 0xf800 && fault_str.size() > 0) {
    // at least one alarm is set
    // remove trailing newline
    fault_str = fault_str.substr(0, fault_str.size()-1);
  } else {
    fault_str = "OK";
  }
  bat[Field::fault_str]->setText(QString::fromStdString(fault_str));

  bat[Field::min_temp]->setText(QString("%1%2C").arg(msg->minPackTemp, 5, 'f', 2).arg(QChar(0260)));
  bat[Field::max_temp]->setText(QString("%1%2C").arg(msg->maxPackTemp, 5, 'f', 2).arg(QChar(0260)));
  bat[Field::max_pack_voltage]->setText(QString("%1 V").arg(msg->maxPackVoltage, 5, 'f', 2));
  bat[Field::min_pack_voltage]->setText(QString("%1 V").arg(msg->minPackVoltage, 5, 'f', 2));
  bat[Field::max_cell_voltage]->setText(QString("%1 V").arg(msg->maxCellVoltage, 5, 'f', 3));
  bat[Field::min_cell_voltage]->setText(QString("%1 V").arg(msg->minCellVoltage, 5, 'f', 3));

  bat[Field::voltage]->setText(QString("%1 V").arg(msg->moduleVoltage, 5, 'f', 2));
  bat[Field::status_code]->setText(QString("0x%1").arg(msg->moduleStatus, 4, 16, QChar('0')));
  bat[Field::percentage]->setText(QString("%1 %").arg(msg->percentFull, 5, 'f', 2));
  bat[Field::remainingCapacity]->setText(QString("%1 Ah").arg(msg->remainingCapacity, 6, 'f', 2));
  bat[Field::fullCapacity]->setText(QString("%1 Ah").arg(msg->fullCapacity, 6, 'f', 2));
  bat[Field::designCapacity]->setText(QString("%1 Ah").arg(msg->designCapacity, 6, 'f', 2));

  bat[Field::chargetimeRemaining]->setText(QString("%1").arg(msg->chargetimeRemaining));
  bat[Field::uptime]->setText(QString("%1").arg(msg->uptime));
  bat[Field::overflowCount]->setText(QString("%1").arg(msg->overflowCount));
  bat[Field::timeoutCount]->setText(QString("%1").arg(msg->timeoutCount));
  bat[Field::startupCondition]->setText(QString("0x%1").arg(msg->startupCondition, 4, 16, QChar('0')));
  bat[Field::date]->setText(QString::fromStdString(msg->date));
  bat[Field::busAddress]->setText(QString("0x%1").arg(msg->busAddress, 4, 16, QChar('0')));
  bat[Field::powerswitchState]->setText(QString("0x%1").arg(msg->powerswitchState, 4, 16, QChar('0')));

  msg_times[batnum] = QDateTime::currentDateTimeUtc();
}

void ClioBatteryModuleTable::updateAges() {
  auto now = QDateTime::currentDateTimeUtc();
  for (size_t m=0; m<fields.size(); m++) {
    if (msg_times[m].isNull()) {
      fields[m][Field::age]->setText(tr("Never"));
      continue;
    }
    // we have to do the math this clunky way because the QTime libraries don't seem to have been designed
    // with arithmatic in mind because.... who knows.
    qint64 msecs_to = msg_times[m].msecsTo(now);
    auto delay = QTime::fromMSecsSinceStartOfDay(msecs_to);

    if (delay.hour() > 0) {
      fields[m][Field::age]->setText(delay.toString("hh:mm:ss.zzz"));
    } else if (delay.minute() > 0) {
      fields[m][Field::age]->setText(delay.toString("mm:ss.zzz"));
    } else {
      fields[m][Field::age]->setText(delay.toString("ss.zzz"));
    }
  }
}

void ClioBatteryModuleTable::setup() {

  layout = new QGridLayout();
  const int MODULE_START_ROW = 1;
  const int MODULE_START_COL = 1;

  // prepare for incoming messages
  msg_times.resize(num_bats);

  // build an empty set of row labels
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  // actually fill out row labels
  row_labels[Field::module_name] = "Module";
  row_labels[Field::status_str] = "Status String";
  row_labels[Field::fault_str] = "Fault String";

  row_labels[Field::min_temp] = "Min Pack Temp";
  row_labels[Field::max_temp] = "Max Pack Temp";
  row_labels[Field::max_pack_voltage] = "Max Pack Voltage";
  row_labels[Field::min_pack_voltage] = "Min Pack Voltage";
  row_labels[Field::max_cell_voltage] = "Max Cell Voltage";
  row_labels[Field::min_cell_voltage] = "Min Cell Voltage";

  row_labels[Field::voltage] = "Module Voltage";
  row_labels[Field::status_code] = "Status Code";
  row_labels[Field::percentage] = "Percentage";
  row_labels[Field::remainingCapacity] = "Capacity, Remaining";
  row_labels[Field::fullCapacity] = "Capacity, Full";
  row_labels[Field::designCapacity] = "Capacity, Design";

  row_labels[Field::chargetimeRemaining] = "Charge Time Remaining";
  row_labels[Field::uptime] = "Uptime";
  row_labels[Field::overflowCount] = "Overflow Count";
  row_labels[Field::timeoutCount] = "Timeout Count";
  row_labels[Field::startupCondition] = "Startup Condition Code";
  row_labels[Field::date] = "Firmware Date";
  row_labels[Field::busAddress] = "Bus Address";
  row_labels[Field::powerswitchState] = "Powerswitch State";
  row_labels[Field::age] = "Message Age";

  // fill out column headers
  headers.resize(Field::NUM_ROWS);
  for (size_t f=0; f < Field::NUM_ROWS; f++) {
    headers[f] = new QLabel(QString::fromStdString(row_labels[f]), this);
    headers[f]->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
    layout->addWidget(headers[f], MODULE_START_ROW + f, 0);
  }

  // fill out modules
  fields.resize(num_bats);
  for (size_t module=0; module < num_bats; module++) {
    fields[module].resize(Field::NUM_ROWS);
    for (size_t f=0; f < Field::NUM_ROWS; f++) {
      fields[module][f] = new QLabel(tr(" "), this);
      fields[module][f]->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Maximum, QSizePolicy::Policy::MinimumExpanding));
      fields[module][f]->setContentsMargins(20, 0, 20, 0);
      fields[module][f]->setAlignment(Qt::AlignCenter);
      layout->addWidget(fields[module][f], MODULE_START_ROW + f, MODULE_START_COL + module);
    }

    // we have to manuall set text for pack IDs
    fields[module][Field::module_name]->setText(QString("Bat %1").arg(module+1));
  }

  layout->setRowStretch(Field::NUM_ROWS, 1);
  layout->setColumnStretch(num_bats, 1);
  this->setLayout(layout);
  this->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
}

} // namespace ds_rqt_widgets
