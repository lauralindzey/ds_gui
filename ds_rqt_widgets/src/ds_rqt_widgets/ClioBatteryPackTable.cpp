

//
// Created by ivaughn on 12/17/19.
//

#include "ds_rqt_widgets/ClioBatteryPackTable.h"

// Marks as bad undervoltage, overtemp, terminate charge, and overcharged alarms
const uint16_t STATUS_MASK = 0xf800;

namespace ds_rqt_widgets {

ClioBatteryPackTable::ClioBatteryPackTable(int n_bats, int n_packs, QWidget *parent)
    : QWidget(parent), num_bats(n_bats), num_packs(n_packs) {
  setup();
}

void ClioBatteryPackTable::updateBattery(AnyMessage clio_bat, int batnum) {
  auto msg = clio_bat.as<ds_hotel_msgs::ClioBattery>();
  if (batnum >= batteries.size()) {
    ROS_ERROR_STREAM("Battery message for battery " <<batnum+1 <<" but only " <<batteries.size() <<" expected by GUI!");
    return;
  }
  auto& bat = batteries[batnum];

  if (msg->packs.size() != bat.packs.size()) {
    ROS_ERROR_STREAM("Battery message for battery " <<batnum+1 <<" has " <<msg->packs.size()
                                                    <<", but that battery only has " <<bat.packs.size() <<" packs expected!");
    return;
  }

  bat.max_temp->setText(QString("%1%2C").arg(msg->maxPackTemp, 5, 'f', 2).arg(QChar(0260)));
  bat.percentage->setText(QString("%1 %").arg(msg->percentFull, 5, 'f', 2));
  bat.max_cell_voltage->setText(QString("%1 V").arg(msg->maxCellVoltage, 5, 'f', 3));
  bat.min_cell_voltage->setText(QString("%1 V").arg(msg->minCellVoltage, 5, 'f', 3));
  bat.voltage->setText(QString("%1 V").arg(msg->moduleVoltage, 5, 'f', 2));
  bat.status_code->setText(QString("0x%1").arg(msg->moduleStatus, 4, 16, QChar('0')));

  for (size_t i=0; i<bat.packs.size(); i++) {
    float min_cell = std::min(std::min(msg->packs[i].lowCellVoltage, msg->packs[i].mid1CellVoltage),
                              std::min(msg->packs[i].mid2CellVoltage, msg->packs[i].highCellVoltage));
    float max_cell = std::max(std::max(msg->packs[i].lowCellVoltage, msg->packs[i].mid1CellVoltage),
                              std::max(msg->packs[i].mid2CellVoltage, msg->packs[i].highCellVoltage));

    bat.packs[i].pack_min_cell_voltage->setText(QString("%1")
                                                    .arg(min_cell, 5, 'f', 3));
    bat.packs[i].pack_max_cell_voltage->setText(QString("%1")
                                                    .arg(max_cell, 5, 'f', 3));
    bat.packs[i].pack_voltage->setText(QString("%1").arg(msg->packs[i].voltage, 5, 'f', 2));
    bat.packs[i].pack_temps->setText(QString("%1%2")
                                         .arg(msg->packs[i].temperature, 5, 'f', 2)
                                         .arg(QChar(0260)));
    bat.packs[i].percentage->setText(QString("%1 %").arg(msg->packs[i].relativeSOC, 3, 'f', 0));
    bat.packs[i].status_code->setText(QString("0x%1").arg(msg->packs[i].status, 4, 16, QChar('0')));
    if (msg->packs[i].status & STATUS_MASK) {
      bat.packs[i].status_code->setStyleSheet("QLabel { background-color: rgb(255 0 0); }");
    } else {
      bat.packs[i].status_code->setStyleSheet("");
    }
  }
}

void ClioBatteryPackTable::setup() {

  layout = new QGridLayout();
  const int HDR_ROWS = 1;
  const int PACK_START = 2;
  const int STRIDE = num_bats+HDR_ROWS;
  int row_header_temp = 1 + STRIDE*0;
  int row_header_percentage = 1 + STRIDE*1;
  int row_header_min_cell_voltage = 1 + STRIDE*2;
  int row_header_max_cell_voltage = 1 + STRIDE*3;
  int row_header_pack_voltage = 1 + STRIDE*4;
  int row_header_status_code = 1 + STRIDE*5;

  // start with some headers
  header_temp = new QLabel(tr("Temperature"), this);
  header_temp->setAlignment(Qt::AlignCenter);
  header_percentage = new QLabel(tr("Percent Full"), this);
  header_percentage->setAlignment(Qt::AlignCenter);
  header_max_cell = new QLabel(tr("Max Cell Voltage"), this);
  header_max_cell->setAlignment(Qt::AlignCenter);
  header_min_cell = new QLabel(tr("Min Cell Voltage"), this);
  header_min_cell->setAlignment(Qt::AlignCenter);
  header_voltage = new QLabel(tr("Total Voltage"), this);
  header_voltage->setAlignment(Qt::AlignCenter);
  header_status_code = new QLabel(tr("Status Code"), this);
  header_status_code->setAlignment(Qt::AlignCenter);
  layout->addWidget(header_temp, row_header_temp, 0, 1, PACK_START + num_packs);
  layout->addWidget(header_percentage, row_header_percentage, 0, 1, PACK_START + num_packs);
  layout->addWidget(header_min_cell, row_header_min_cell_voltage, 0, 1, PACK_START + num_packs);
  layout->addWidget(header_max_cell, row_header_max_cell_voltage, 0, 1, PACK_START + num_packs);
  layout->addWidget(header_voltage, row_header_pack_voltage, 0, 1, PACK_START + num_packs);
  layout->addWidget(header_status_code, row_header_status_code, 0, 1, PACK_START + num_packs);

  batteries.resize(num_bats);
  for (size_t i=0; i<num_bats; i++) {
    auto& bat = batteries[i];

    // module overall
    bat.max_temp = new QLabel(tr("? %"), this);
    bat.percentage = new QLabel(tr("? %"), this);
    bat.max_cell_voltage = new QLabel(tr("? V"), this);
    bat.min_cell_voltage = new QLabel(tr("? V"), this);
    bat.voltage = new QLabel(tr("? V"), this);
    bat.status_code = new QLabel(tr("?"), this);
    layout->addWidget(bat.max_temp, row_header_temp + i + HDR_ROWS, 0);
    layout->addWidget(bat.percentage, row_header_percentage + i + HDR_ROWS, 0);
    layout->addWidget(bat.min_cell_voltage, row_header_min_cell_voltage + i + HDR_ROWS, 0);
    layout->addWidget(bat.max_cell_voltage, row_header_max_cell_voltage + i + HDR_ROWS, 0);
    layout->addWidget(bat.voltage, row_header_pack_voltage + i + HDR_ROWS, 0);
    layout->addWidget(bat.status_code, row_header_status_code + i + HDR_ROWS, 0);

    // setup module label
    for (size_t ii=0; ii<bat.module_name.size(); ii++) {
      bat.module_name[ii] = new QLabel(QString("Bat %1").arg(i + 1, 1));
    }
    layout->addWidget(bat.module_name[0], row_header_temp + i + HDR_ROWS, 1);
    layout->addWidget(bat.module_name[1], row_header_percentage + i + HDR_ROWS, 1);
    layout->addWidget(bat.module_name[2], row_header_min_cell_voltage + i + HDR_ROWS, 1);
    layout->addWidget(bat.module_name[3], row_header_max_cell_voltage + i + HDR_ROWS, 1);
    layout->addWidget(bat.module_name[4], row_header_pack_voltage + i + HDR_ROWS, 1);
    layout->addWidget(bat.module_name[5], row_header_status_code + i + HDR_ROWS, 1);

    // pack individual packs
    bat.packs.resize(num_packs);
    for (size_t p=0; p<num_packs; p++) {
      bat.packs[p].pack_min_cell_voltage = new QLabel(tr("?"), this);
      bat.packs[p].pack_max_cell_voltage = new QLabel(tr("?"), this);
      bat.packs[p].pack_voltage = new QLabel(tr("?"), this);
      bat.packs[p].pack_temps = new QLabel(tr("?"), this);
      bat.packs[p].percentage = new QLabel(tr("?"), this);
      bat.packs[p].status_code = new QLabel(tr("?"), this);

      layout->addWidget(bat.packs[p].pack_temps, row_header_temp + i + HDR_ROWS, p+PACK_START);
      layout->addWidget(bat.packs[p].percentage, row_header_percentage + i + HDR_ROWS, p+PACK_START);
      layout->addWidget(bat.packs[p].pack_min_cell_voltage, row_header_min_cell_voltage + i + HDR_ROWS, p+PACK_START);
      layout->addWidget(bat.packs[p].pack_max_cell_voltage, row_header_max_cell_voltage + i + HDR_ROWS, p+PACK_START);
      layout->addWidget(bat.packs[p].pack_voltage, row_header_pack_voltage + i + HDR_ROWS, p+PACK_START);
      layout->addWidget(bat.packs[p].status_code, row_header_status_code + i + HDR_ROWS, p+PACK_START);
    }
  }

  this->setLayout(layout);
}

} // namespace ds_rqt_widgets
