//
// Created by ivaughn on 11/4/20.
//

#ifndef DS_RQT_WIDGETS_INDICATOR_BUTTON_GROUP_H
#define DS_RQT_WIDGETS_INDICATOR_BUTTON_GROUP_H

#include "IndicatorButton.h"
#include <QGridLayout>
#include <QGroupBox>
#include <functional>

namespace ds_rqt_widgets {

class IndicatorButtonGroup : public QGroupBox {
  Q_OBJECT

 public:
  IndicatorButtonGroup(const std::string& long_name, const std::string& short_name,
                       std::function<void(int)>cb, QWidget* parent=NULL);

  void addButton(const std::string& name, int enumval);

  void updateMode(int value);
  void setUnknown();

 protected:
  std::string groupName;
  std::vector<std::pair<int, IndicatorButton*>> buttons;
  QGridLayout* layout;
  std::function<void(int)> callback;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_INDICATOR_BUTTON_GROUP_H
