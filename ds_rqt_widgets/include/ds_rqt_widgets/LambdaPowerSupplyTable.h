//
// Created by ivaughn on 10/7/20.
//

#ifndef DS_RQT_WIDGETS_LAMBDAPOWERSUPPLYTABLE_H
#define DS_RQT_WIDGETS_LAMBDAPOWERSUPPLYTABLE_H

#include "SensorTableBase.h"
#include <ds_hotel_msgs/PowerSupply.h>

namespace ds_rqt_widgets {

class LambdaPowerSupplyTable : public SensorTableBase {
  Q_OBJECT

 public:
  explicit LambdaPowerSupplyTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updatePowerSupply(AnyMessage powersupply);

 private:
  ds_hotel_msgs::PowerSupply msg;
};
} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_LAMBDAPOWERSUPPLYTABLE_H
