//
// Created by ivaughn on 12/27/19.
//

#ifndef DS_RQT_WIDGETS_PHINSTABLE_H_
#define DS_RQT_WIDGETS_PHINSTABLE_H_

#include "SensorTableBase.h"
#include <ds_sensor_msgs/PhinsStdbin3.h>

namespace ds_rqt_widgets {

class PhinsTable : public SensorTableBase {
  Q_OBJECT
 public:
  explicit PhinsTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updatePhinsbin(AnyMessage phinsbin);

 private:
  ds_sensor_msgs::PhinsStdbin3 msg;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_PHINSTABLE_H_
