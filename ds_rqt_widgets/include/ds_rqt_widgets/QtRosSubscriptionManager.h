//
// Created by ivaughn on 2/4/19.
//

#ifndef PROJECT_QTROSSUBSCRIPTIONMANAGER_H
#define PROJECT_QTROSSUBSCRIPTIONMANAGER_H

#include "QtRosSubscription.h"

#include <qapplication.h>
#include <qt_gui_cpp/settings.h>

namespace ds_rqt_widgets {

class QtRosSubscriptionManager : public QObject {
  Q_OBJECT;
  Q_DISABLE_COPY(QtRosSubscriptionManager);

 public:
  QtRosSubscriptionManager(ros::NodeHandle& nh);
  virtual ~QtRosSubscriptionManager();

  template <typename MSG>
  QtRosSubscription* makeSubscription(ros::NodeHandle &nh, const std::string& name,
    const std::string &topicname, uint32_t queue_size = 10) {
    QtRosSubscription* ret = SubscribeToTopic<MSG>(nh, name, topicname, queue_size);
    addSubscription(ret);
    return ret;
  }

  void addSubscription(QtRosSubscription* sub);
  void shutdown();

  void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
  void restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                       const qt_gui_cpp::Settings& instance_settings);

  const std::vector<QtRosSubscription*>& getSubscriptions() const;

 protected:
  std::vector<QtRosSubscription*> subscriptions;
  ros::NodeHandle node_handle;

  QString topicSettingName(const QtRosSubscription * const sub) const;

};

}

#endif //PROJECT_QTROSSUBSCRIPTIONMANAGER_H
