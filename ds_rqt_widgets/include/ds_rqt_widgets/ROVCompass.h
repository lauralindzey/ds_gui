#ifndef ROVCOMPASS_H
#define ROVCOMPASS_H

#include <QtGui>
#include <QWidget>

#include <QPainter>
#include <QColor>

#define  MAX_HISTORY 256
#define  COMPASS_SIZE   190

namespace ds_rqt_widgets {

typedef enum {
  NORTH_UP,
  VEHICLE_UP
} compassType_t;

class ROVCompass : public QWidget {
  Q_OBJECT
 public:
  explicit ROVCompass(QWidget *parent = 0);
  void setHeadingValue(double theValue);
  void setHistory(qreal *historyPointer, int lengthOfHistory);
  void setGoalValue(double theValue);
  void setRate(double theRate);
  void setHeadingAndRate(double theHeading, double theRate);
  void refresh();

  signals:

 private:
  double heading;
  double headingSetPoint;
  double headingRate;

  bool northUp;

  compassType_t compassType;

  void drawBackground();
  void drawValueNeedle();
  void drawSetPointNeedle();

  qreal history[MAX_HISTORY];
  int historyLength;
  void drawHistory();

 public
  slots:

 protected:
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  QSize sizeHint() const { return QSize(COMPASS_SIZE, COMPASS_SIZE); }

};

}

#endif // ROVCOMPASS_H
