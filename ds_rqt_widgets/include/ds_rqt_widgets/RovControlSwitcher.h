//
// Created by ivaughn on 2/22/19.
//

#ifndef DS_RQT_WIDGETS_CONTROL_SWITCHER_H
#define DS_RQT_WIDGETS_CONTROL_SWITCHER_H

#include <QWidget>
#include <QPushButton>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <ds_control_msgs/RovAutoCommand.h>
#include <ds_control_msgs/RovControllerState.h>
#include <ds_core_msgs/Abort.h>

#include "ds_param/ds_param.h"
#include "ds_param/ds_param_conn.h"

#include "QtRosSubscription.h"

#include "IndicatorButton.h"
#include "IndicatorButtonGroup.h"

namespace ds_rqt_widgets {

class RovControlSwitcher : public QWidget {
  Q_OBJECT

 public:
  explicit RovControlSwitcher(QWidget* parent=0);

  void setupConnections(
    const ds_param::IntParam::Ptr& _joystick,
    const ds_param::IntParam::Ptr& _controller,
    const ds_param::IntParam::Ptr& _allocator);

  void setupAutoService(const ros::ServiceClientPtr& client);

  // accessors to add your own custom modes!
  IndicatorButtonGroup* joysticks();
  IndicatorButtonGroup* controllers();
  IndicatorButtonGroup* allocations();

 public slots:

  void updateModes();
  void enableAutos();
  void disableAutos();

  void setJoystick(int joy);
  void setController(int con);
  void setAllocation(int alloc);

  void updateAbort(AnyMessage raw_msg);
  void updateControllerState(AnyMessage raw_msg);

 signals:
  void setVehicleEnable(bool);
  void setVehicleAbort(bool);

 protected:
  void setupGui();

  QGroupBox* groupAbort;
  QGridLayout* layoutAbort;
  IndicatorButton* butEnable;
  IndicatorButton* butAbort;

  QGroupBox* groupAutos;
  QGridLayout* layoutAutos;
  IndicatorButton* butAutoOff;
  IndicatorButton* butAutoXY;
  IndicatorButton* butAutoHdg;
  IndicatorButton* butAutoDepth;

  IndicatorButtonGroup* butsJoysticks;
  IndicatorButtonGroup* butsControllers;
  IndicatorButtonGroup* butsAllocations;

  QVBoxLayout* widgetLayout;

  ds_param::IntParam::Ptr active_joystick;
  ds_param::IntParam::Ptr active_controller;
  ds_param::IntParam::Ptr active_allocator;
  ros::ServiceClientPtr auto_service;

  ds_control_msgs::RovControllerState auto_state;
  bool auto_enabled;
  bool auto_received;

  void callbackAutoXy();
  void callbackAutoDep();
  void callbackAutoHdg();
  void callbackAutoOff();

  void updateAutos();
};

}

#endif //DS_RQT_WIDGETS_CONTROL_SWITCHER_H
