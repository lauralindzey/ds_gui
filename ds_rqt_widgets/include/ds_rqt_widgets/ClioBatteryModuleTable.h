//
// Created by ivaughn on 12/17/19.
//

#ifndef DS_RQT_WIDGETS_CLIOBATTERYTABLE_H
#define DS_RQT_WIDGETS_CLIOBATTERYTABLE_H

#include <QtGui>
#include <QWidget>
#include <QLabel>
#include <QGridLayout>

#include <ds_hotel_msgs/BatMan.h>
#include <ds_hotel_msgs/ClioBattery.h>
#include <ds_hotel_msgs/ClioBatteryPack.h>
#include "QtRosSubscription.h"

namespace ds_rqt_widgets {


class ClioBatteryModuleTable : public QFrame {
 Q_OBJECT
 public:
  explicit ClioBatteryModuleTable(int num_bats, QWidget *parent=0);

 public slots:
  void updateBattery(AnyMessage clio_bat, int batnum);
  void updateAges();

 protected:
  void setup();

 private:
  int num_bats; // number of battery modules installed

  QGridLayout* layout;
  std::vector<std::vector<QLabel*>> fields;
  std::vector<QLabel*> headers;
  std::vector<QDateTime> msg_times;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_CLIOBATTERYTABLE_H
