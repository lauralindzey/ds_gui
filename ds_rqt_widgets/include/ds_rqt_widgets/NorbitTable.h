//
// Created by ivaughn on 12/27/19.
//

#ifndef DS_RQT_WIDGETS_NORBITTABLE_H_
#define DS_RQT_WIDGETS_NORBITTABLE_H_

#include "SensorTableBase.h"
#include <ds_multibeam_msgs/NorbitMB.h>
#include <ds_multibeam_msgs/MultibeamRaw.h>

namespace ds_rqt_widgets {

    class NorbitTable : public SensorTableBase {
        Q_OBJECT
    public:
        explicit NorbitTable(const std::string& name, QWidget *parent=0);

    protected:
        void setup();

    public slots:
                void refresh();
        void updateNorbit(AnyMessage norbit);

    private:
        ds_multibeam_msgs::NorbitMB msg;
    };

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_NORBITTABLE_H_
