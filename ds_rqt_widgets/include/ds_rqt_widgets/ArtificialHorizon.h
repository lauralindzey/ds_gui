#ifndef ARTIFICIAL_HORIZON_H
#define ARTIFICIAL_HORIZON_H

#include <QtWidgets>

namespace ds_rqt_widgets {

#define ATTITUDE_COLOR_BROWN_LIGHT QColor(164,147,108)
#define ATTITUDE_COLOR_BROWN_DARK QColor(106,91,60)
#define ATTITUDE_COLOR_BLUE_LIGHT QColor(161,189,255)
#define ATTITUDE_COLOR_BLUE_DARK QColor(98,116,165)
#define JH_BROWN QColor(212,140,95)


class ArtificialHorizon : public QWidget
{
   Q_OBJECT
public:
   explicit ArtificialHorizon(QWidget *parent = 0);
   void            setPitch(double theValue);
   void            setRoll(double theValue);
   void            refresh();

   
signals:

private:
   double         pitch;
   double         roll;


   void            drawBackground();
   void            drawPitchLines();







   
public slots:

protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void paintEvent (QPaintEvent  *event);

   
};

}

#endif // ARTIFICIAL_HORIZON_H
