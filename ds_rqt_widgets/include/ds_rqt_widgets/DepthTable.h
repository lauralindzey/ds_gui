//
// Created by ivaughn on 12/27/19.
//

#ifndef DS_RQT_WIDGETS_DEPTHTABLE_H_
#define DS_RQT_WIDGETS_DEPTHTABLE_H_

#include "SensorTableBase.h"
#include <ds_sensor_msgs/DepthPressure.h>

namespace ds_rqt_widgets {

class DepthTable : public SensorTableBase {
  Q_OBJECT
 public:
  explicit DepthTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updateDepth(AnyMessage depth);

 private:
  ds_sensor_msgs::DepthPressure msg;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_DEPTHTABLE_H_
