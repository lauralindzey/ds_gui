//
// Created by ivaughn on 2/4/19.
//

#ifndef PROJECT_QTROSSUBSCRIPTIONWIDGET_H
#define PROJECT_QTROSSUBSCRIPTIONWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>

#include "QtRosSubscriptionManager.h"

namespace ds_rqt_widgets {

class QtRosSubscriptionWidget : public QWidget {
  Q_OBJECT
 public:
  QtRosSubscriptionWidget(QWidget* parent, ros::NodeHandle& nh, QtRosSubscriptionManager* subs);
  virtual ~QtRosSubscriptionWidget();

  // TODO
 protected:
  void setupWidget();

  // we DO NOT own our subscription manager; NEVER delete it.
  ros::NodeHandle node_handle;
  QtRosSubscriptionManager* sub_manager;

  std::vector<QLabel*> sub_name_labels;
  std::vector<QComboBox*> sub_topics;
  QPushButton* refresh_but;
  QGridLayout* layout;

  void onTopicChanged(const QString& text, QtRosSubscription* sub);

 public slots:
  void refreshTopics();
};

class QtRosSubscriptionPopout : public QWidget {
  Q_OBJECT
 public:
  QtRosSubscriptionPopout(QWidget* parent, ros::NodeHandle& nh, QtRosSubscriptionManager* subs);

 public slots:
  void popup_create();
  void popup_close();

 private:
  QtRosSubscriptionWidget* widget;

  ros::NodeHandle node_handle;
  QtRosSubscriptionManager* sub_manager;
};


}
#endif //PROJECT_QTROSSUBSCRIPTIONWIDGET_H
