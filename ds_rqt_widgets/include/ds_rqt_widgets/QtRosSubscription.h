//
// Created by ivaughn on 2/1/19.
//

#ifndef PROJECT_QTROSSUBSCRIPTION_H
#define PROJECT_QTROSSUBSCRIPTION_H

#include <boost/shared_ptr.hpp>
#include <boost/any.hpp>
#include <boost/function.hpp>

#include <ros/ros.h>

#include <QObject>
#include <QMetaType>
#include <QTime>

/// This is a lovely little module that presents a ROS subscription as a QT signal generator.
/// We use a number of cute tricks to erase the type and get around the fact that QT objects
/// are the only ones allowed to have signals and slots but can't use templates.

/// This is a generic base class message used to erase the type
/// of individual messages while in transport.  It has some special sauce to get properly
/// registered with QT as a payload for multithreaded signal transport
///
/// This class is a VERY thin wrapper around boost::any.  During initial testing, the author
/// found a lot of issues with getting const correct.  Since this approach is very brittle to
/// EXTREMELY minor typing errors, and tends to produce non-sensical error messages, this
/// class was added to make things easier.
///
/// In general, template parameters for this class should ONLY be the message type--
/// e.g., std_msgs::String-- and NOT a reference, const message type, pointer message type,
/// etc.  We handle all that for you.  Just tell the function what message you want, and
/// you shall have it!
///
/// Also, QT just can't handle putting this class in a namespace without being a jerk about it.
class AnyMessage {
 public:
  AnyMessage() = default;
  virtual ~AnyMessage();

  /// Create a new wrapper around an incoming message
  /// \tparam T The underlying message type; this will be erased shortly.
  /// \param msg The message to save
  template<typename T>
  explicit AnyMessage(T msg) {
    payload = msg;
  }

  /// Get a shared pointer to the ROS message payload of this container
  /// \tparam T The requested type of the ROS message
  /// \return A const shared pointer to the ROS message
  /// \throws boost::bad_any_cast if the cast fails
  template<typename T>
  const boost::shared_ptr<const T> as() const {
    return boost::any_cast<const boost::shared_ptr<const T> >(payload);
  }

  /// Get a modifyable version of this message
  /// \tparam T The requested type of the ROS message
  /// \return A shared pointer to a fresh copy of the ROS message
  /// \throws boost::bad_any_cast if the cast fails
  template<typename T>
  boost::shared_ptr<T> copy() const {
    auto ptr = boost::any_cast<const boost::shared_ptr<const T> >(payload);
    return boost::shared_ptr<T>(new T(*ptr));
  }

  /// Get the type info for the underlying payload object
  const boost::typeindex::type_info &type() const noexcept;

 private:
  // has type const boost::shared_ptr<const T>
  boost::any payload;
};

Q_DECLARE_METATYPE(AnyMessage);

namespace ds_rqt_widgets {

/// This is an abstract base class for subscriptions.  End users should NOT use this
/// class directly.
class GenericSubscription {
 public:
  typedef boost::function<void(AnyMessage)> FunctionType;

  GenericSubscription(const std::string& _name);
  virtual ~GenericSubscription() = 0;

  void setCallback(const boost::function<void(AnyMessage)> &cb);

  virtual std::vector<std::string> getTopics(ros::NodeHandle& nh) const = 0;
  virtual void shutdown();
  std::string getTopicName() const;
  virtual void setTopicName(ros::NodeHandle& nh, const std::string& topicname, uint32_t queue_size) = 0;
  uint32_t getNumPublishers() const;
  ros::WallTime getLastMessageTime() const;
  const std::string& getName() const;

 protected:
  boost::function<void(AnyMessage)> callback;
  ros::Subscriber subscription;
  std::string name;
  ros::WallTime last_message_time;
};

/// This is a templated subclass for subscriptions.
/// \tparam T The type of the message to subscribe to.  This MUST be JUST the
/// message type-- no const, pointers, *, shared_ptrs, etc.
template<typename T>
class RosSubscription : public GenericSubscription {

 public:
  RosSubscription(ros::NodeHandle &nh, const std::string& _name,
      const std::string &topicname, uint32_t queue_size) : GenericSubscription(_name) {
    subscription = nh.subscribe(topicname, queue_size, &RosSubscription::on_msg, this);
  }

  /// this callback erases the type of the message and passes it on to the Qt object
  void on_msg(const boost::shared_ptr<T const> &msg) {
    last_message_time = ros::WallTime::now();
    if (callback) {
      callback(AnyMessage(msg));
    } else {
      ROS_DEBUG("No callback specified for QtRosSubscription! (this shouldn't be possible)");
    }
  }

  std::vector<std::string> getTopics(ros::NodeHandle& nh) const override {
    std::vector<std::string> ret;
    ros::master::V_TopicInfo topics;
    ros::master::getTopics(topics);

    // Oh yeah! Undocumented rostypes feature!
    std::string typestr = ros::message_traits::DataType<T>::value();

    for (auto topic : topics) {
      if (topic.datatype == typestr) {
        std::cout << topic.datatype << "    :    " << topic.name << "\n";
        ret.push_back(topic.name);
      }
    }

    return ret;
  }

  void setTopicName(ros::NodeHandle& nh, const std::string& topicname, uint32_t queue_size) override {
    subscription.shutdown();
    subscription = nh.subscribe(topicname, queue_size, &RosSubscription::on_msg, this);
  }
};

/// This is our QT object that has a public slot that fires every time we get a message
/// There are a LOT of things it'd be nice to add here-- notably rate filtering!
class QtRosSubscription : public QObject {

 Q_OBJECT;
  Q_DISABLE_COPY(QtRosSubscription);
 public:
  QtRosSubscription(GenericSubscription *s);
  virtual ~QtRosSubscription();

  std::vector<std::string> getTopics(ros::NodeHandle& nh) const;

  std::string getName() const;
  std::string getTopicName() const;
  void setTopicName(ros::NodeHandle& nh, const std::string& topicname, uint32_t queue_size=10);

  uint32_t getNumPublishers() const;
  ros::WallTime getLastMessageTime_ros() const;
  QDateTime getLastMessageTime_QT() const;
  void shutdown();

 signals:
  /// This signal is fired any time a message is received on this topic.
  /// You should use this one!
  /// HOWEVER! You MUST use a Qt::QueuedConnection
  /// The explanation for this is long and annoying.  Basically,
  /// QT connections can be invoked as either direct connections or queued
  /// connections.
  void received(AnyMessage msg);

 private:
  GenericSubscription *sub;
  void on_data(AnyMessage msg);
};

/// This is how new QtRosSubscriptions get made.  This function takes a single
/// template parameter in the type, and spits out a valid subscription with
/// a signal that gets fired whenever a new message arrives
/// \param nh The node handle to use when creating the subscription object.
/// \param name The name of this setting to use in strings, etc
/// \param topicname The topic to subscribe to.
/// \param queue_size The queue size to use for the underlying subscription.  Defaults to 10.
/// \return A QtRosSubscription object ready for your use
/// \tparam T The type of the message to subscribe to.  This MUST be JUST the
/// message type-- no const, pointers, *, shared_ptrs, etc.
template<typename T>
QtRosSubscription *SubscribeToTopic(ros::NodeHandle &nh, const std::string& name,
    const std::string &topicname, uint32_t queue_size = 10) {
  qRegisterMetaType<AnyMessage>();
  return new QtRosSubscription(new RosSubscription<T>(nh, name, topicname, queue_size));
}

}



#endif //PROJECT_QTROSSUBSCRIPTION_H
