//
// Created by ivaughn on 12/11/18.
//

#include "ds_raw_module.h"

#include <pluginlib/class_list_macros.h>
#include <ros/master.h>

#include <sstream>
#include <iomanip>
#include <boost/date_time.hpp>
#include <cctype>
#include <QtGui>



namespace ds_raw_console {

RawViewerPlugin::RawViewerPlugin() : rqt_gui_cpp::Plugin(), widget_(0) {
  ROS_INFO_STREAM("Raw viewer INITIALIZED!");
  setObjectName("RawViewerPlugin");
  hexmode_ = false;
  paused_ = false;
}

void RawViewerPlugin::initPlugin(qt_gui_cpp::PluginContext& context) {
  ROS_INFO_STREAM("init raw console plugin");

  widget_ = new QWidget();

  setupWidget();

  context.addWidget(widget_);
}

void RawViewerPlugin::shutdownPlugin() {
  ROS_INFO_STREAM("Shutdown connections!");

  // unregister all publishers, subscribers, etc
  shutdownConnections();

}

void RawViewerPlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings,
                                      qt_gui_cpp::Settings& instance_settings) const {
  ROS_INFO_STREAM("Saving current topic " <<topics_->currentText().toStdString());
  instance_settings.setValue("current_topic", topics_->currentText());

}
void RawViewerPlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings) {

  QString topic = instance_settings.value("current_topic", topics_->currentText()).toString();
  topics_->addItem(topic);
  topics_->setCurrentText(topic);
  ROS_INFO_STREAM("Restoring topic " <<topic.toStdString());

  setupConnections();
}

void RawViewerPlugin::setupWidget() {

  // First, the topics stuff
  topics_ = new QComboBox(widget_);
  refreshTopics_ = new QPushButton("Refresh", widget_);
  refreshTopics_->setFixedWidth(70);
  refreshTopics_->setMaximumWidth(70);
  topicLayout_ = new QHBoxLayout();
  topicLayout_->addWidget(topics_);
  topicLayout_->addWidget(refreshTopics_);

  // Terminal box in the middle
  terminalBox_ = new QPlainTextEdit(widget_);
  terminalBox_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  terminalBox_->setReadOnly(true);
  QTextDocument* doc = terminalBox_->document();
  QFont font = doc->defaultFont();
  font.setFamily("Courier New");
  doc->setDefaultFont(font);

  // Bottom options row
  clearBut_ = new QPushButton("Clear", widget_);
  optionsSpacer_ = new QSpacerItem(10, 10, QSizePolicy::Ignored, QSizePolicy::Minimum);
  hexBox_ = new QCheckBox("Hex", widget_);
  pauseBox_ = new QCheckBox("Pause", widget_);
  optionsLayout_ = new QHBoxLayout();
  optionsLayout_->addWidget(clearBut_);
  optionsLayout_->addSpacerItem(optionsSpacer_);
  optionsLayout_->addWidget(hexBox_);
  optionsLayout_->addWidget(pauseBox_);


  // Stitch it all together
  topLayout_ = new QVBoxLayout();
  topLayout_->addLayout(topicLayout_);
  topLayout_->addWidget(terminalBox_);
  topLayout_->addLayout(optionsLayout_);

  // wire up our connections
  connect(topics_, SIGNAL(currentIndexChanged(QString)), this, SLOT(setTopic(QString)));
  connect(refreshTopics_, SIGNAL(released()), this, SLOT(refreshTopics()));
  connect(clearBut_, SIGNAL(released()), this, SLOT(clearText()));
  connect(hexBox_, SIGNAL(stateChanged(int)), this, SLOT(setHexMode(int)));
  connect(pauseBox_, SIGNAL(stateChanged(int)), this, SLOT(setPaused(int)));

  connect(this, SIGNAL(textReady(QString)), this, SLOT(addText(QString)));

  widget_->setLayout(topLayout_);
}

void RawViewerPlugin::setupConnections() {
  // We don't actually setup our connections; just refresh
  refreshTopics();
}

void RawViewerPlugin::shutdownConnections() {
  raw_sub.shutdown();
}

void RawViewerPlugin::raw_data_in(const ds_core_msgs::RawData &data) {
  // If we're paused, just drop data
  if (paused_) {
    return;
  }

  // print the timestamp, but forgo date
  const boost::posix_time::time_duration &time = data.ds_header.io_time.toBoost().time_of_day();
  std::stringstream output;
  output << std::setw(2) << std::setfill('0') << time.hours() << ":";
  output << std::setw(2) << std::setfill('0') << time.minutes() << ":";
  output << std::setw(2) << std::setfill('0') << time.seconds() << ".";
  output << std::setw(6) << std::setfill('0') << data.ds_header.io_time.nsec/1000 <<" ";


  // Add the direction indicator
  if (data.data_direction == ds_core_msgs::RawData::DATA_IN) {
    output <<"RX ";
  } else if (data.data_direction == ds_core_msgs::RawData::DATA_OUT) {
    output <<"TX ";
  } else {
    output <<"?? ";
  }

  // render the actual data
  if (hexmode_) {
    for (size_t i=0; i<data.data.size(); i++) {
      output <<std::setw(2) <<std::setfill('0') <<std::hex <<static_cast<int>(data.data[i]);
      if (i % 4 == 0) {
        output <<" ";
      }
    }
  } else {
    // go through one at a time and escape unprintable characters
    for (unsigned char c : data.data) {
      switch (c) {
        case '\0': output <<"\\0"; break;
        case '\b': output <<"\\b"; break;
        case '\t': output <<"\\t"; break;
        case '\n': output <<"\\n"; break;
        case '\v': output <<"\\v"; break;
        case '\f': output <<"\\f"; break;
        case '\r': output <<"\\r"; break;
        case '\\': output <<"\\\\"; break;
        default:
          if (isprint(c)) {
            output <<c;
          } else {
            output << "\\x" << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(c);
          }
      }
    }
  }

  // emit the result as a QT event to neatly work around multithreading issues
  QString toAdd = QString::fromStdString(output.str());
  emit textReady(toAdd);
}

void RawViewerPlugin::addText(QString text) {
  terminalBox_->appendPlainText(text);
}

void RawViewerPlugin::setTopic(QString topic) {
  std::string topicStr = topic.toStdString();
  ROS_INFO_STREAM("RawViewerPlugin: Connecting to topic " <<topicStr);

  // disconnect any possible existing connection
  if (raw_sub && raw_sub.getTopic() == topicStr) {
    ROS_INFO_STREAM("Already connected to topic, skipping...");
    // already connected, do nothing
    return;
  }

  // terminate the existing connection
  raw_sub.shutdown();

  if (topicStr.empty()) {
    ROS_INFO_STREAM("RawViewerPlugin: Attempted to connect to empty topic, skipping...");
    return;
  }
  // actually subscribe
  ros::NodeHandle nh = getMTNodeHandle();
  raw_sub = nh.subscribe(topicStr, 10, &RawViewerPlugin::raw_data_in, this);
}

void RawViewerPlugin::clearText() {
  terminalBox_->clear();
}

void RawViewerPlugin::setHexMode(int checked) {
  hexmode_ = checked;
}

void RawViewerPlugin::setPaused(int checked) {
  paused_ = checked;
}

void RawViewerPlugin::refreshTopics() {
  ROS_INFO("RawViewerPlugin: Refereshing list of ds_core_msgs/RawData topics...");

  // first, stash the current topic
  QString current = topics_->currentText();

  // get the list of topics
  ros::master::V_TopicInfo topicList;
  if (!ros::master::getTopics(topicList) ) {
    ROS_ERROR_STREAM("RawViewerPlugin: Unable to get a list of topics!");
    return;
  }

  // go through the list and add anything that's a RawData message
  bool currentSeen=false;
  QStringList rawTopics;
  for (const ros::master::TopicInfo& t : topicList) {
    if (t.datatype == "ds_core_msgs/RawData") {
      rawTopics.push_back(QString::fromStdString(t.name));
      if (QString::fromStdString(t.name) == current) {
        currentSeen = true;
      }
    }
  }

  // sort, to bring some semblance of sanity
  rawTopics.sort(Qt::CaseInsensitive);

  // always ensure we have the current topic after refreshing
  if (!currentSeen) {
    rawTopics.push_front(current);
  }

  // update the combo box
  topics_->clear();
  topics_->addItems(rawTopics);
  topics_->setCurrentText(current);
}


}

PLUGINLIB_EXPORT_CLASS(ds_raw_console::RawViewerPlugin, rqt_gui_cpp::Plugin);
