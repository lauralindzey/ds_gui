# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import os
import rospy
import rospkg
import threading
from ds_hotel_msgs.msg import PowerSupply
# from sensor_msgs.msg import BatteryState

from qt_gui.plugin import Plugin
# from python_qt_binding import loadUi
#from python_qt_binding.QtGui import QWidget
# PyQt5 change, it appears:
from PyQt5 import QtCore, QtGui, QtWidgets
from python_qt_binding.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout, QGridLayout

class PowerSupplyPlugin(Plugin):

    def __init__(self, context):
        super(PowerSupplyPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('PowerSupplyPlugin')

        self.time_since = 0

        print 'PowerSupply PLUGIN LOADED'

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                            dest="quiet",
                            help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        self._init_widgets()

        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))

        context.add_widget(self._widget)

        self._PowerSupply_subscription = rospy.Subscriber("/demo/bat/chg3/dcpwr", PowerSupply, self.PowerSupply_callback)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.time_callback)
        self.timer.start(1000)

    def PowerSupply_callback(self, pwr):
        i = 1

        self.labelid[i].setText( str(pwr.idnum))
        self.labelVset[i].setText( str( round(pwr.prog_volts, 3)))
        self.labelVmeas[i].setText( str( round(pwr.meas_volts, 3)))
        self.labelCset[i].setText( str( round(pwr.prog_amps, 3)))
        self.labelCmeas[i].setText( str( round(pwr.meas_amps, 3)))

        if pwr.frontpanel_locked :
            self.labelLocked[i].setText("Locked")
        else :
            self.labelLocked[i].setText("Unlocked")

        if pwr.output_enable :
            self.labelOutput[i].setText("Enabled")
        else:
            self.labelOutput[i].setText("Disabled")

        self.time_since[i] = 0
        self.labeltime[i].setText( str(self.time_since[i]))
        self.labeltime[i].show()


    def time_callback(self):
        for i in range( len(self.time_since)):
            self.time_since[i] += 1
            if self.time_since > 1:
                self.labeltime[i].setText( str(self.time_since[i]))

    def _init_widgets(self):
        # Create QWidget
        self._widget = QWidget()

        self.grid = QGridLayout(self._widget)

        self.verticalSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.grid.addItem(self.verticalSpacer, 7, 0)

        self.labelid = []
        self.labelVset = []
        self.labelVmeas = []
        self.labelCset = []
        self.labelCmeas = []
        self.labelLocked = []
        self.labelOutput = []
        self.labeltime = []
        self.time_since = []

        self.newfont = QtGui.QFont("Times", 14, QtGui.QFont.Bold)
        self.add_powersupply_display("Lambda ID:", "V set:", "V meas:", "C set:", "C meas:", "Frontpanel:", "Output:")
        self.newfont = QtGui.QFont("Times", 24, QtGui.QFont.Bold)
        self.add_powersupply_display("", "", "", "", "", "", "")

        self._widget.setObjectName('PowerSupplyPluginUi')

    def add_powersupply_display(self, strid, strVset, strVmeas, strCset, strCmeas, strL, strOut):

        num = len(self.time_since)

        self.labelid.append(QLabel(strid, self._widget))
        self.labelVset.append(QLabel(strVset, self._widget))
        self.labelVmeas.append(QLabel(strVmeas, self._widget))
        self.labelCset.append(QLabel(strCset, self._widget))
        self.labelCmeas.append(QLabel(strCmeas, self._widget))
        self.labelLocked.append(QLabel(strL, self._widget))
        self.labelOutput.append(QLabel(strOut, self._widget))
        self.labeltime.append(QLabel("", self._widget))

        self.grid.addWidget(self.labelid[num], 0, num)
        self.grid.addWidget(self.labelVset[num], 1, num)
        self.grid.addWidget(self.labelVmeas[num], 2, num)
        self.grid.addWidget(self.labelCset[num], 3, num)
        self.grid.addWidget(self.labelCmeas[num], 4, num)
        self.grid.addWidget(self.labelLocked[num], 5, num)
        self.grid.addWidget(self.labelOutput[num], 6, num)
        self.grid.addWidget(self.labeltime[num], 8, num)

        self.labelid[num].setFont(self.newfont)
        self.labelVset[num].setFont(self.newfont)
        self.labelVmeas[num].setFont(self.newfont)
        self.labelCset[num].setFont(self.newfont)
        self.labelCmeas[num].setFont(self.newfont)
        self.labelLocked[num].setFont(self.newfont)
        self.labelOutput[num].setFont(self.newfont)
        self.labeltime[num].setFont(self.newfont)

        self.labeltime[num].hide()

        self.time_since.append(0)

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        self._PowerSupply_subscription.unregister()
        self.timer.stop()

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
    # Comment in to signal that the plugin has a way to configure
    # This will enable a setting button (gear icon) in each dock widget title bar
    # Usually used to open a modal configuration dialog

