# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import rospy
import rospkg

from ds_core_msgs.msg import RawData
import datetime
import codecs
import binascii

import termcolor
import itertools

def splitEvery(w,n):
    for i in range(0, len(w),n):
        yield w[i:i+n]

class RawConsole(object):
    def __init__(self, _topicname, encode_hex=False, hex_spacing=0):
        self.topicname = _topicname
        self._sub = rospy.Subscriber(self.topicname, RawData, self._callback)
        self._encode_hex = encode_hex
        self._hex_spacing = hex_spacing


    def _callback(self, msg):

        # Build the time
        dt = datetime.datetime.utcfromtimestamp(msg.ds_header.io_time.to_sec())
        timestr = dt.strftime('%H:%M:%S.%f')[0:-3]

        # Get the direction for color, etc
        if msg.data_direction == RawData.DATA_IN:
            dirstr = 'RX'
            color = 'cyan'
        else:
            dirstr = 'TX'
            color = 'magenta'

        # Encode the string as requested
        if self._encode_hex:
            msgstr = binascii.hexlify(msg.data)
            msglen = len(msg.data)
            if self._hex_spacing > 0:
                spacing = 2*self._hex_spacing
                msgstr = ' '.join(splitEvery(msgstr, spacing))
        else:
            msgstr,msglen = codecs.escape_encode(msg.data)


        # Finally, output!
        termcolor.cprint('%12s %2s %s' % (timestr, dirstr, msgstr), color)
