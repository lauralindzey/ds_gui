from raw_console import RawConsole
import sys
import rospy
import rosgraph

import argparse

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # filter out extra arguments if launched via roslaunch
    argv = rospy.myargv(argv)
    if len(argv) < 2:
        print 'MUST specify a topic on the command line'
        sys.exit(-1)

    parser = argparse.ArgumentParser(description="Listen to a RawData topic and watch the data go in/out")
    parser.add_argument('-x', '--hex', action='store_true', help='Display data as hex instead of escaped ascii')
    parser.add_argument('-s', '--hex-spacing', type=int, default=0, help='Number of bytes to group when displaying hex')
    parser.add_argument('topic', help='Topic to listen on.  Type MUST be RawData', nargs=1);

    args = parser.parse_args(args=argv[1:])

    rospy.init_node('raw_console', anonymous=True)
    topic = rosgraph.names.script_resolve_name('raw_console', args.topic[0])

    print 'Listening on topic \"%s\"' % topic
    console = RawConsole(topic, encode_hex=args.hex, hex_spacing=args.hex_spacing)
    rospy.spin()


